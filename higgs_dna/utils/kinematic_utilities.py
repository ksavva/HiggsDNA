import numpy as np
import awkward
from coffea.nanoevents.methods import vector


def calculate_delta_phi(phi1, phi2):
    """
    Calculate the difference in azimuthal angles while handling angle wrapping.

    Parameters:
        phi1 (array-like): Azimuthal angles of the first object.
        phi2 (array-like): Azimuthal angles of the second object.

    Returns:
        array: Array of calculated delta_phi values.
    """

    delta_phi = phi1 - phi2
    delta_phi = (delta_phi + np.pi) % (2 * np.pi) - np.pi
    return delta_phi


def calculate_transverse_mass(obj1, obj2):
    """
    Calculate the transverse mass for object pairs.

    Parameters:
        obj1 (awkward.array): Awkward Array containing first object information (pt and phi).
        obj2 (awkward.array): Awkward Array containing second object information (pt and phi).

    Returns:
        numpy.array: Array of calculated transverse mass values.
    """

    delta_phi = calculate_delta_phi(obj1["phi"], obj2["phi"])
    cos_delta_phi = np.cos(delta_phi)
    mt2 = 2 * obj1["pt"] * obj2["pt"] * (1 - cos_delta_phi)
    return np.sqrt(mt2)


def calculate_transverse_momentum(obj1, obj2):
    """
    Calculate the transverse momentum of two objects.

    Parameters:
        obj1 (awkward.array): Awkward Array containing information for the first object (pt and phi).
        obj2 (awkward.array): Awkward Array containing information for the second object (pt and phi).

    Returns:
        numpy.array: Array of calculated transverse momentum values.
    """

    delta_phi = calculate_delta_phi(obj1.phi, obj2.phi)
    pt = np.sqrt(obj1.pt**2 + obj2.pt**2 + 2 * obj1.pt * obj2.pt * np.cos(delta_phi))

    return pt


def calculate_p4(obj: awkward.Array) -> awkward.Array:
    return awkward.zip(
        {
            "pt": obj.pt,
            "eta": obj.eta,
            "phi": obj.phi,
            "mass": obj.mass,
            "pdgId": obj.pdgId if "pdgId" in obj.fields else -9999.0 * awkward.ones_like(obj.pt),
            "charge": obj.charge if "charge" in obj.fields else -9999.0 * awkward.ones_like(obj.pt),
        },
        with_name="PtEtaPhiMLorentzVector",
        behavior=vector.behavior
    )


def sum_p4(obj, axis):
    p4_cartesian = awkward.zip(
        {
            "x": obj.x,
            "y": obj.y,
            "z": obj.z,
            "t": obj.t,
        },
        with_name="LorentzVector",
        behavior=vector.behavior
    )

    sum_p4_cartesian = awkward.sum(p4_cartesian, axis=axis)
    sum_p4_cartesian = awkward.with_name(sum_p4_cartesian, "LorentzVector")

    sum_p4_PtEtaPhiM = awkward.zip(
        {
            "pt": sum_p4_cartesian.pt,
            "eta": sum_p4_cartesian.eta,
            "phi": sum_p4_cartesian.phi,
            "mass": sum_p4_cartesian.mass,
        },
        with_name="PtEtaPhiMLorentzVector",
        behavior=vector.behavior
    )

    return sum_p4_PtEtaPhiM
