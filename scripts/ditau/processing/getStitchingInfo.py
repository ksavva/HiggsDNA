import os
import yaml
import argparse


def parse_arguments():
    parser = argparse.ArgumentParser(description="Process .txt files in specified subdirectories and create a YAML file.")
    parser.add_argument('--year', type=str)
    return parser.parse_args()


def create_yaml(data, output_file):
    with open(output_file, 'w') as file:
        yaml.dump(data, file, default_flow_style=False)


def main():
    args = parse_arguments()
    output_file = os.path.join(os.getcwd(), f'scripts/ditau/config/{args.year}/Stitching.yaml')

    # open yaml file to get cross-sections
    input_xs = os.path.join(os.getcwd(), 'scripts/ditau/config/cross_sections.yaml')
    with open(input_xs) as file:
        cross_sections = yaml.load(file, Loader=yaml.FullLoader)

    input_eff = os.path.join(os.getcwd(), f'scripts/ditau/config/{args.year}/effective_events.yaml')
    with open(input_eff) as file:
        effective_events = yaml.load(file, Loader=yaml.FullLoader)

    Run_ID = args.year.split('_')[0]

    # Define processes to include
    samples = {
        'DYto2L_M-50_madgraphMLM': 'Inclusive',
        'DYto2L_M-50_madgraphMLM_ext1': 'Inclusive',
        'DYto2L_M-50_1J_madgraphMLM': '1J',
        'DYto2L_M-50_2J_madgraphMLM': '2J',
        'DYto2L_M-50_3J_madgraphMLM': '3J',
        'DYto2L_M-50_4J_madgraphMLM': '4J',
        'WtoLNu_madgraphMLM': 'Inclusive',
        'WtoLNu_madgraphMLM_ext1': 'Inclusive',
        'WtoLNu_1J_madgraphMLM': '1J',
        'WtoLNu_2J_madgraphMLM': '2J',
        'WtoLNu_3J_madgraphMLM': '3J',
        'WtoLNu_4J_madgraphMLM': '4J',
    }
    data = {"DY": {}, "WJ": {}}

    for sample in samples.keys():
        category = samples[sample]
        process = 'DY' if 'DY' in sample else 'WJ'
        if sample in effective_events:
            number = effective_events[sample]
        else:
            number = 0
        if category not in data[process]:
            data[process][category] = {"Effective_Events": number}
        else:
            data[process][category]["Effective_Events"] += number

        cross_section = cross_sections[Run_ID]['processes'][process]['cross_sections'][sample]
        if category in data[process] and "Cross_section" not in data[process][category]:
            data[process][category]["Cross_section"] = cross_section

    create_yaml(data, output_file)
    print(f"YAML file '{output_file}' created successfully.")


if __name__ == "__main__":
    main()
