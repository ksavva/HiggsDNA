import numpy as np
import awkward as ak
import correctionlib
import os
from copy import deepcopy


def Tau_EnergyScale(events, year, is_correction=True):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL2016_preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL2016_postVFP"),
        "Run2_2017": ("2017_UL", "UL2017"),
        "Run2_2018": ("2018_UL", "UL2018"),
        "Run3_2022": ("2022_preEE", "2022_preEE"),
        "Run3_2022EE": ("2022_postEE", "2022_postEE"),
    }

    folder_name, file_name = year_mapping.get(year, (None, None))
    json_name = "tau_energy_scale"
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/TauID/{folder_name}/tau_DeepTau2018v2p5_{file_name}.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for TAU ES.")

    name = "DeepTau2018v2p5"
    wp_VSjet = "Loose"
    wp_VSe = "VVLoose"

    counts = ak.num(events.Tau.pt)

    tau_4vec = ak.zip(
        {
            "pt": ak.flatten(events.Tau.pt),
            "eta": ak.flatten(events.Tau.eta),
            "phi": ak.flatten(events.Tau.phi),
            "mass": ak.flatten(events.Tau.mass),
        },
        with_name="PtEtaPhiMLorentzVector",
    )

    mass = ak.flatten(events.Tau.mass)
    dm = ak.flatten(events.Tau.decayMode)
    gen = ak.flatten(events.Tau.genPartFlav)

    # Masking the taus with decay modes 5 and 6  to avoid correctionlib crashing
    mask = (dm == 5) | (dm == 6)
    masked_dm = np.where(mask, 0, dm)

    if is_correction:
        correction = evaluator.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_dm, gen, name, wp_VSjet, wp_VSe, "nom")

        corrected_px = np.where((dm != 5) | (dm != 6),tau_4vec.px * correction, tau_4vec.px)
        corrected_py = np.where((dm != 5) | (dm != 6),tau_4vec.py * correction, tau_4vec.py)
        corrected_pz = np.where((dm != 5) | (dm != 6),tau_4vec.pz * correction, tau_4vec.pz)
        corrected_M = np.where((dm != 0) | (dm != 5) | (dm != 6),mass * correction, mass)

        corrected_tau_3vec = ak.zip(
            {
                "x": corrected_px,
                "y": corrected_py,
                "z": corrected_pz,
            },
            with_name="Momentum3D",
        )

        corrected_pt = np.sqrt(corrected_tau_3vec.x**2 + corrected_tau_3vec.y**2)
        corrected_eta = corrected_tau_3vec.pseudorapidity
        corrected_phi = corrected_tau_3vec.phi

        corrected_taus = deepcopy(events.Tau)
        corrected_taus["pt"] = ak.unflatten(corrected_pt, counts)
        corrected_taus["eta"] = ak.unflatten(corrected_eta, counts)
        corrected_taus["phi"] = ak.unflatten(corrected_phi, counts)
        corrected_taus["mass"] = ak.unflatten(corrected_M, counts)

        events["Tau"] = corrected_taus

        return events

    else:

        uncertainty_up = evaluator.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_dm, gen, name, wp_VSjet, wp_VSe, "up")
        uncertainty_down = evaluator.evaluate(ak.flatten(events.Tau.pt), ak.flatten(events.Tau.eta), masked_dm, gen, name, wp_VSjet, wp_VSe, "down")

        corrected_px_up = np.where((dm != 5) | (dm != 6),tau_4vec.px * uncertainty_up, tau_4vec.px)
        corrected_py_up = np.where((dm != 5) | (dm != 6),tau_4vec.py * uncertainty_up, tau_4vec.py)
        corrected_pz_up = np.where((dm != 5) | (dm != 6),tau_4vec.pz * uncertainty_up, tau_4vec.pz)
        corrected_M_up = np.where((dm != 0) | (dm != 5) | (dm != 6),mass * uncertainty_up, mass)

        corrected_tau_3vec_up = ak.zip(
            {
                "x": corrected_px_up,
                "y": corrected_py_up,
                "z": corrected_pz_up,
            },
            with_name="Momentum3D",
        )

        corrected_pt_up = np.sqrt(corrected_tau_3vec_up.x**2 + corrected_tau_3vec_up.y**2)
        corrected_eta_up = corrected_tau_3vec_up.pseudorapidity
        corrected_phi_up = corrected_tau_3vec_up.phi

        corrected_taus_up = deepcopy(events.Tau)
        corrected_taus_up["pt"] = ak.unflatten(corrected_pt_up, counts)
        corrected_taus_up["eta"] = ak.unflatten(corrected_eta_up, counts)
        corrected_taus_up["phi"] = ak.unflatten(corrected_phi_up, counts)
        corrected_taus_up["mass"] = ak.unflatten(corrected_M_up, counts)

        corrected_px_down = np.where((dm != 5) | (dm != 6),tau_4vec.px * uncertainty_down, tau_4vec.px)
        corrected_py_down = np.where((dm != 5) | (dm != 6),tau_4vec.py * uncertainty_down, tau_4vec.py)
        corrected_pz_down = np.where((dm != 5) | (dm != 6),tau_4vec.pz * uncertainty_down, tau_4vec.pz)
        corrected_M_down = np.where((dm != 0) | (dm != 5) | (dm != 6),mass * uncertainty_down, mass)

        corrected_tau_3vec_down = ak.zip(
            {
                "x": corrected_px_down,
                "y": corrected_py_down,
                "z": corrected_pz_down,
            },
            with_name="Momentum3D",
        )

        corrected_pt_down = np.sqrt(corrected_tau_3vec_down.x**2 + corrected_tau_3vec_down.y**2)
        corrected_eta_down = corrected_tau_3vec_down.pseudorapidity
        corrected_phi_down = corrected_tau_3vec_down.phi

        corrected_taus_down = deepcopy(events.Tau)
        corrected_taus_down["pt"] = ak.unflatten(corrected_pt_down, counts)
        corrected_taus_down["eta"] = ak.unflatten(corrected_eta_down, counts)
        corrected_taus_down["phi"] = ak.unflatten(corrected_phi_down, counts)
        corrected_taus_down["mass"] = ak.unflatten(corrected_M_down, counts)

        return corrected_taus_up, corrected_taus_down
