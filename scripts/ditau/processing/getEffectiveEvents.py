import os
import argparse
import yaml


def read_number_from_txt(file_path):
    with open(file_path, 'r') as file:
        number = file.read().strip()
    return number


def process_directory(directory, year):
    results = {}
    # Get a list of all subdirectories
    directory = os.path.join(directory, year)
    subdirectories = [os.path.join(directory, d) for d in os.listdir(directory) if os.path.isdir(os.path.join(directory, d))]

    # Process each subdirectory
    for subdir in subdirectories:
        total_sum = 0
        # Get a list of all files in the subdirectory
        for file in os.listdir(subdir):
            if file.endswith('.txt') and not file == 'effective_numbers.txt':
                file_path = os.path.join(subdir, file)
                number = int(read_number_from_txt(file_path))
                total_sum += number

        # Write the total sum to effective_numbers.txt
        output_file_path = os.path.join(subdir, 'effective_numbers.txt')
        with open(output_file_path, 'w') as output_file:
            output_file.write(str(total_sum))

        subdir_name = os.path.basename(subdir)
        results[subdir_name] = total_sum

    yaml_output_path = f'scripts/ditau/config/{year}/effective_events.yaml'
    with open(yaml_output_path, 'w') as yaml_file:
        yaml.dump(results, yaml_file)
    print("Effective events have been written to", yaml_output_path)


def main():
    parser = argparse.ArgumentParser(description="Process subdirectories to sum numbers in text files.")
    parser.add_argument('--directory', type=str, help='The directory to look into for subdirectories')
    parser.add_argument('--year', type=str, help='The year to process')

    args = parser.parse_args()
    process_directory(args.directory,args.year)


if __name__ == '__main__':
    main()
