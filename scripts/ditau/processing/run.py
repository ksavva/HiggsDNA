# example command to run:
# python3 ditau/EffectiveEvents/dryrun_stitching.py --json-analysis ditau/config/ditau_analysis.json --output output/effective_events

import json
import argparse
import subprocess


def update_json_analysis(json_analysis, updates):
    """
    Updates the specified fields in the JSON file with given values.
    :param json_analysis: Path to the JSON file.
    :param updates: Dictionary containing fields and their new values.
    """
    try:
        with open(json_analysis, 'r') as file:
            data = json.load(file)

        for key, value in updates.items():
            data[key] = value

        with open(json_analysis, 'w') as file:
            json.dump(data, file, indent=4)

    except Exception as e:
        print(f"Error updating JSON analysis file: {e}")


def parse_args():
    parser = argparse.ArgumentParser(description="Run analysis and stitching commands.")
    parser.add_argument('--json-analysis', required=False, help='Path to the JSON analysis file.')
    parser.add_argument('--year', required=False, help='Year of the analysis')
    parser.add_argument('--output', required=False, help='Output directory.')
    parser.add_argument('--channels', required=False, help='Channels to run the analysis on.')
    parser.add_argument('--dir_jobs', required=False, help='Directory with the jobs.')
    parser.add_argument('--step', required=True, help='Step to run.')
    parser.add_argument('--batch', action='store_true', help='Send jobs to batch.')

    args = parser.parse_args()

    if (args.step == "run_effective" or args.step == "standard") and not args.json_analysis:
        parser.error("--json-analysis is required for run_effective and standard steps.")
    elif args.step == "resubmit" and not args.dir_jobs:
        parser.error("--dir_jobs is required for resubmit step.")
    elif args.step in ["run_effective", "standard", "effectiveEvents"] and not args.output:
        parser.error("--output is required for run_effective, standard and effectiveEvents steps.")

    return args


def run_command(command):
    try:
        print(f"Running command: {command}")
        subprocess.run(command, shell=True, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Error running command: {e}")


def main():
    args = parse_args()

    # check if args.json_analysis is provided, if yes, get the year
    if args.json_analysis:
        with open(args.json_analysis, 'r') as file:
            data = json.load(file)
            year = data["year"]
        if args.year:
            assert args.year == year, "Year in the JSON file and the argument do not match."
    else:
        year = args.year

    # Define the commands
    command_analyser = (
        f"python3 scripts/ditau/processing/run_analysis.py --json-analysis {args.json_analysis} "
        f"--dump {args.output} --executor imperial_condor --voms ~/cms.proxy "
        f"--chunk 500000 --debug"
    )

    command_testAnalyser = (
        "python3 scripts/ditau/processing/run_analysis.py --json-analysis scripts/ditau/config/test/test_analysis.json "
        "--dump output/testAnalyser --executor iterative --voms ~/cms.proxy --chunk 50000 --max 1 --debug "
    )

    command_resubmitJobs = (
        f"python3 scripts/ditau/processing/resubmit_jobs_condor.py {args.dir_jobs}"
    )

    command_getEffectiveEvents = (
        f'python3 scripts/ditau/processing/getEffectiveEvents.py --directory {args.output} --year {year}'
    )

    command_getStitchingInfo = (
        f"python3 scripts/ditau/processing/getStitchingInfo.py --year {year}"
    )

    command_getParams = (
        f"python3 scripts/ditau/processing/getParams.py --year {year}"
    )

    command_mergeParquet = (
        f"python3 scripts/ditau/post_processing/merge_parquet.py --parent_dir {args.output} --years {year} --channels {args.channels} --use_condor"
    )

    command_parquetToRoot = (
        f"python3 scripts/ditau/post_processing/convert_parquet_to_root.py --parent_dir {args.output} --years {year} --channels {args.channels} --use_condor"
    )

    if args.step == "run_effective":
        updates = {
            "Run_Effective": True,
            "EventsNotSelected": False,
        }
        update_json_analysis(args.json_analysis, updates)
        run_command(command_analyser)
        updates = {
            "Run_Effective": True,
            "EventsNotSelected": True,
        }
        update_json_analysis(args.json_analysis, updates)
        if not args.batch:
            command_analyser = command_analyser.replace("--executor imperial_condor", "--executor iterative")
        run_command(command_analyser)
    elif args.step == "resubmit":
        run_command(command_resubmitJobs)
    elif args.step == "effectiveEvents":
        run_command(command_getEffectiveEvents)
    elif args.step == "stitching":
        run_command(command_getStitchingInfo)
    elif args.step == "params":
        run_command(command_getParams)
    elif args.step == "standard":
        updates = {
            "Run_Effective": False,
            "EventsNotSelected": False,
        }
        update_json_analysis(args.json_analysis, updates)
        if not args.batch:
            command_analyser = command_analyser.replace("--executor imperial_condor", "--executor iterative")
        run_command(command_analyser)
    elif args.step == "testAnalyser":
        run_command(command_testAnalyser)
    elif args.step == "merge":
        if not args.batch:
            command_mergeParquet = command_mergeParquet.replace("--use_condor", "")
        run_command(command_mergeParquet)
    elif args.step == "parquetToRoot":
        if not args.batch:
            command_parquetToRoot = command_parquetToRoot.replace("--use_condor", "")
        run_command(command_parquetToRoot)


if __name__ == "__main__":
    main()
