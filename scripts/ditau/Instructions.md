## Instructions

Renew grid certificate:

```
source /vols/grid/cms/setup.sh
voms-proxy-init --rfc --voms cms --valid 192:00 --out ${HOME}/cms.proxy
export X509_USER_PROXY=${HOME}/cms.proxy
```

## Create a json file containing list of files

NanoProd skims are stored on the EOS Higgs PAG space. The list of samples to look for is provided in this `scripts/ditau/pre_processing/samples.yaml`. The samples in the .yaml file are separated based on the campaign (year) and type (data, signal, background).

Create the json file containing the sample paths using:

```
python3 scripts/ditau/pre_processing/fetch_samples.py
```

Output: E.g. output of this command --> `scripts/ditau/config/$YEAR$/samples.json`

## Effective Event Information

### Step 1: Quick Running of the analyser to get effective event numbers

The analyser now has dry-run options to get the effective event numbers and create the stitching config with the relevant cross-sections and effective event numbers for each year. The following command will submit jobs to HTCondor for both trees (Events & EventsNotSelected). This means that two job directories are created i.e. `.higgs_dna_jobs/output_25_06_2024/effective/Run3_2022/Events` and `.higgs_dna_jobs/output_25_06_2024/effective/Run3_2022/EventsNotSelected` as an example.

```
python scripts/ditau/processing/run.py --json-analysis scripts/ditau/config/ditau_analysis.json --output output/effective --step run_effective --batch
```

### Step 2: Resubmit any failed jobs

Note: job_dir needs to be replaced by your job directory.

To resubmit any failed jobs:

```
python scripts/ditau/processing/run.py --json-analysis scripts/ditau/config/ditau_analysis.json --dir_jobs .higgs_dna_jobs/output_25_06_2024/effective/Run3_2022/Events/jobs/ --step resubmit
```

### Step 3: Get Effective Events for each sample

The next step is to get the actual effective events for each sample. The following command will sum up all the effective numbers for each sample (stored in \*.txt files in the corresponding sample directory in the output folder). The output will be an `effective_events.txt` file for each sample with the total effective event number.

To get effective events:

```
python3 scripts/ditau/processing/run.py --json-analysis scripts/ditau/config/ditau_analysis.json --output output/effective --step effectiveEvents

OR

python3 scripts/ditau/processing/run.py --year ADD_YEAR_HERE --output output/effective --step effectiveEvents
```

### Step 4: Get Stitching Information

The following command automatically creates the config file required to for stitching. It utilises the `scripts/ditau/config/cross_sections.yaml` file along with the effective numbers calculated in the last step. The output is stored in `scripts/ditau/config/..year../Stitching.yaml`

To get stitching information:

```
python3 scripts/ditau/processing/run.py --json-analysis scripts/ditau/config/ditau_analysis.json --step stitching

OR

python3 scripts/ditau/processing/run.py --year ADD_YEAR_HERE --step stitching
```

### Step 5: Get Parameters (Lumi, XS, Effective Event Numbers in YAML file)

```
python3 scripts/ditau/processing/run.py --year ADD_YEAR_HERE --step params
```

### Step 6: Run the "standard" workflow

```
python3 scripts/ditau/processing/run.py --json-analysis scripts/ditau/config/ditau_analysis.json --output output/testxx --step standard --batch
```

### Step 7: Run a single analyser file

```
python3 scripts/ditau/processing/run.py --step testAnalyser
```

### Step 8: Merge Parquet output files from the workflow

```
python3 scripts/ditau/processing/run.py --json-analysis scripts/ditau/config/ditau_analysis.json --output output/testxx --channels mm --step merge

OR

python3 scripts/ditau/processing/run.py --year ADD_YEAR_HERE --output output/testxx --channels mm --step merge

```

### Step 9: Morph merged parquet output into a root file

```
python3 scripts/ditau/processing/run.py --json-analysis scripts/ditau/config/ditau_analysis.json --output output/testxx --channels mm --step parquetToRoot

OR

python3 scripts/ditau/processing/run.py --year ADD_YEAR_HERE --output output/testxx --channels mm --step parquetToRoot

```
