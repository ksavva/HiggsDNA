{
  "name": "tau_es_dm_UL2018",
  "description": "DM-dependent tau energy scale in UL2018, to be applied to reconstructed tau_h Lorentz vector (pT, mass and energy) in simulated data",
  "version": 0,
  "inputs": [
    { "name": "pt",
      "type": "real",
      "description": "Reconstructed tau pT"
    },
    { "name": "eta",
      "type": "real",
      "description": "Reconstructed tau eta"
    },
    { "name": "dm",
      "type": "int",
      "description": "Reconstructed tau decay mode: 0, 1, 2, 10, 11"
    },
    { "name": "genmatch",
      "type": "int",
      "description": "genmatch: 0 or 6 = unmatched or jet, 1 or 3 = electron, 2 or 4 = muon, 5 = real tau"
    },
    { "name": "id",
      "type": "string",
      "description": "Tau ID: DeepTau2017v2p1"
    },
    { "name": "syst",
      "type": "string",
      "description": "Systematic variation: 'nom', 'up', 'down'"
    }
  ],
  "output": {
    "name": "tes",
    "type": "real",
    "description": "tau energy scale"
  },
  "data": {
    "nodetype": "category",
    "input": "id",
    "content": [
      { "key": "DeepTau2017v2p1",
        "value": {
          "nodetype": "transform",
          "input": "genmatch",
          "rule": {
            "nodetype": "category",
            "input": "genmatch",
            "content": [
              { "key": 0, "value": 6.0 },
              { "key": 1, "value": 1.0 },
              { "key": 2, "value": 2.0 },
              { "key": 3, "value": 1.0 },
              { "key": 4, "value": 2.0 },
              { "key": 5, "value": 5.0 },
              { "key": 6, "value": 6.0 }
            ]
          },
          "content": {
            "nodetype": "category",
            "input": "genmatch",
            "content": [
              { "key": 1,
                "value": {
                  "nodetype": "category",
                  "input": "dm",
                  "content": [
                    { "key": 0,
                      "value": {
                        "nodetype": "transform",
                        "input": "eta",
                        "rule": {
                          "nodetype": "formula",
                          "expression": "abs(x)",
                          "parser": "TFormula",
                          "variables": [ "eta" ]
                        },
                        "content": {
                          "nodetype": "binning",
                          "input": "eta",
                          "edges": [ 0.0, 1.5, 2.5 ],
                          "content": [
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 1.01362 },
                                { "key": "up", "value": 1.02266 },
                                { "key": "down", "value": 1.00888 }
                              ]
                            },
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 0.96903 },
                                { "key": "up", "value": 1.00307 },
                                { "key": "down", "value": 0.95653 }
                              ]
                            }
                          ],
                          "flow": "clamp"
                        }
                      }
                    },
                    { "key": 1,
                      "value": {
                        "nodetype": "transform",
                        "input": "eta",
                        "rule": {
                          "nodetype": "formula",
                          "expression": "abs(x)",
                          "parser": "TFormula",
                          "variables": [ "eta" ]
                        },
                        "content": {
                          "nodetype": "binning",
                          "input": "eta",
                          "edges": [ 0.0, 1.5, 2.5 ],
                          "content": [
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 1.01945 },
                                { "key": "up", "value": 1.03171 },
                                { "key": "down", "value": 1.00347 }
                              ]
                            },
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 0.985 },
                                { "key": "up", "value": 1.03999 },
                                { "key": "down", "value": 0.94191 }
                              ]
                            }
                          ],
                          "flow": "clamp"
                        }
                      }
                    },
                    { "key": 2,
                      "value": {
                        "nodetype": "transform",
                        "input": "eta",
                        "rule": {
                          "nodetype": "formula",
                          "expression": "abs(x)",
                          "parser": "TFormula",
                          "variables": [ "eta" ]
                        },
                        "content": {
                          "nodetype": "binning",
                          "input": "eta",
                          "edges": [ 0.0, 1.5, 2.5 ],
                          "content": [
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 1.01945 },
                                { "key": "up", "value": 1.03171 },
                                { "key": "down", "value": 1.00347 }
                              ]
                            },
                            { "nodetype": "category",
                              "input": "syst",
                              "content": [
                                { "key": "nom", "value": 0.985 },
                                { "key": "up", "value": 1.03999 },
                                { "key": "down", "value": 0.94191 }
                              ]
                            }
                          ],
                          "flow": "clamp"
                        }
                      }
                    },
                    { "key": 10, "value": 1.0 },
                    { "key": 11, "value": 1.0 }
                  ]
                }
              },
              { "key": 2,
                "value": {
                  "nodetype": "category",
                  "input": "syst",
                  "content": [
                    { "key": "nom", "value": 1.0 },
                    { "key": "up", "value": 1.01 },
                    { "key": "down", "value": 0.99 }
                  ]
                }
              },
              { "key": 5,
                "value": {
                  "nodetype": "category",
                  "input": "dm",
                  "content": [
                    { "key": 0,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 0.991 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.999,
                                { "nodetype": "formula",
                                  "expression": "0.9935+0.000161765*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.021
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.983,
                                { "nodetype": "formula",
                                  "expression": "0.9885-0.000161765*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.961
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    },
                    { "key": 1,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 1.004 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                1.01,
                                { "nodetype": "formula",
                                  "expression": "1.0065+0.000102941*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.024
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.998,
                                { "nodetype": "formula",
                                  "expression": "1.0015-0.000102941*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.984
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    },
                    { "key": 2,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 1.004 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                1.01,
                                { "nodetype": "formula",
                                  "expression": "1.0065+0.000102941*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.024
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.998,
                                { "nodetype": "formula",
                                  "expression": "1.0015-0.000102941*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.984
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    },
                    { "key": 10,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 0.998 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                1.005,
                                { "nodetype": "formula",
                                  "expression": "1.004+2.94118e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.009
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.991,
                                { "nodetype": "formula",
                                  "expression": "0.992-2.94118e-05*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.987
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    },
                    { "key": 11,
                      "value": {
                        "nodetype": "category",
                        "input": "syst",
                        "content": [
                          { "key": "nom", "value": 1.004 },
                          { "key": "up",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                1.013,
                                { "nodetype": "formula",
                                  "expression": "1.0055+0.000220588*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                1.043
                              ],
                              "flow": "clamp"
                            }
                          },
                          { "key": "down",
                            "value": {
                              "nodetype": "binning",
                              "input": "pt",
                              "edges": [ 0.0, 34.0, 170.0, 1000.0 ],
                              "content": [
                                0.995,
                                { "nodetype": "formula",
                                  "expression": "1.0025-0.000220588*x",
                                  "parser": "TFormula",
                                  "variables": [ "pt" ]
                                },
                                0.965
                              ],
                              "flow": "clamp"
                            }
                          }
                        ]
                      }
                    }
                  ]
                }
              },
              { "key": 6, "value": 1.0 }
            ]
          }
        }
      }
    ]
  }
}
