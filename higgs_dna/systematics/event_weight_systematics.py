import numpy as np
import json
import os
from scipy.interpolate import interp1d
import correctionlib
import awkward as ak
from higgs_dna.utils.misc_utils import choose_jet
import uproot
import logging

logger = logging.getLogger(__name__)


def Pileup(events, weights, year, is_correction=True, **kwargs):
    """
    Function to apply either the pileup correction to MC to make it match the pileup profile of a certain year/period,
    or the respective uncertainties.
    The parameter `year` needs to be specified as one of ["Run3_2022", "Run3_2022EE"] for Run-3 or ["Run2_2016","Run2_2016_HIPM","Run2_2017","Run2_2018"] for Run-2.
    """

    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "Collisions16_UltraLegacy_goldenJSON"),
        "Run2_2016": ("2016postVFP_UL", "Collisions16_UltraLegacy_goldenJSON"),
        "Run2_2017": ("2017_UL", "Collisions17_UltraLegacy_goldenJSON"),
        "Run2_2018": ("2018_UL", "Collisions18_UltraLegacy_goldenJSON"),
        "Run3_2022": ("2022_Summer22", "Collisions2022_355100_357900_eraBCD_GoldenJson"),
        "Run3_2022EE": ("2022_Summer22EE", "Collisions2022_359022_362760_eraEFG_GoldenJson"),
        "Run3_2023": ("2023_Summer23", "Collisions2023_366403_369802_eraBC_GoldenJson"),
        "Run3_2023BPix": ("2023_Summer23BPix", "Collisions2023_369803_370790_eraD_GoldenJson"),
    }

    folder_name, json_name = year_mapping.get(year, (None, None))
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/pileup/{folder_name}/puWeights.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for pileup reweighting.")

    if is_correction:
        sf = evaluator.evaluate(events.Pileup.nPU, "nominal")
        sfup, sfdown = None, None
    else:
        sf = np.ones(len(weights._weight))
        sf_nom = evaluator.evaluate(events.Pileup.nPU, "nominal")
        sfup = evaluator.evaluate(events.Pileup.nPU, "up") / sf_nom
        sfdown = evaluator.evaluate(events.Pileup.nPU, "down") / sf_nom

    weights.add(name="Pileup", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Zpt_Reweighting(pair, weights, year, dataset_name, is_correction=True, **kwargs):
    if dataset_name in ["DYto2L_M-10to50_madgraphMLM","DYto2L_M-50_madgraphMLM","DYto2L_M-50_madgraphMLM_ext1","DYto2L_M-50_1J_madgraphMLM","DYto2L_M-50_2J_madgraphMLM","DYto2L_M-50_3J_madgraphMLM","DYto2L_M-50_4J_madgraphMLM"]:
        if year not in ["Run3_2022"]:
            raise ValueError(f"Year {year} not supported for Zpt reweighting.")
        else:
            path_to_root = os.path.join(os.path.dirname(__file__), f"ROOT/Zpt/{year}/zpt_reweighting_LO.root")
            evaluator = uproot.open(path_to_root)["zptmass_histo"]
            if is_correction:

                z_pt = pair['obj_1'].matched_gen.parent.parent.pt
                z_mass = pair['obj_1'].matched_gen.parent.parent.mass
                z_pt = ak.fill_none(z_pt, 0.0)
                z_mass = ak.fill_none(z_mass, 0.0)
                z_pt = np.asarray(z_pt)
                z_mass = np.asarray(z_mass)

                corrections, x_edges, y_edges = evaluator.to_numpy()
                x_indices = np.searchsorted(x_edges, z_mass, side="right") - 1
                y_indices = np.searchsorted(y_edges, z_pt, side="right") - 1
                x_indices = np.clip(x_indices, 0, len(x_edges) - 2)
                y_indices = np.clip(y_indices, 0, len(y_edges) - 2)

                sf = corrections[x_indices, y_indices]
                sfup, sfdown = None, None
            # TODO: Uncertainties for Zpt reweighting?
    else:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

    weights.add(name="Zpt_Reweighting", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Tau_ID(pair,channel,weights,year,is_correction=True, **kwargs):

    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL2016_preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL2016_postVFP"),
        "Run2_2017": ("2017_UL", "UL2017"),
        "Run2_2018": ("2018_UL", "UL2018"),
        "Run3_2022": ("2022_preEE", "2022_preEE"),
        "Run3_2022EE": ("2022_postEE", "2022_postEE"),
    }

    folder_name, file_name = year_mapping.get(year, (None, None))
    json_name = "DeepTau2018v2p5VSjet"
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/TauID/{folder_name}/tau_DeepTau2018v2p5_{file_name}.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for TAU ID.")

    year = file_name
    wp_VSjet = "Medium"
    wp_VSe = "VVLoose"

    systematics_map = {
        "stat1": "stat1_dM$DM",
        "stat2": "stat2_dM$DM",
        "syst_all_eras": "syst_all_eras",
        "syst_era": "syst_$ERA",
        "syst_TES_era_dm": "syst_TES_$ERA_dm$DM"
    }

    all_variations = [
        "stat1_1",
        "stat1_2",
        "stat2_1",
        "stat2_2",
        "syst_all_eras_1",
        "syst_all_eras_2",
        "syst_era_1",
        "syst_era_2",
        "syst_TES_era_dm_1",
        "syst_TES_era_dm_2"
    ]

    if is_correction:
        sf = np.ones(len(weights._weight))
        for index in range(len(channel)):
            pnumber = index + 1
            if channel[index] == "t":
                sf *= evaluator.evaluate(
                    pair[f'obj_{pnumber}'].pt, pair[f'obj_{pnumber}'].decayMode, pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, "nom", "dm"
                )

        sfs_up = [None for _ in all_variations]
        sfs_down = [None for _ in all_variations]

    else:
        variations = {}
        for syst_name in all_variations:
            variations[syst_name] = {}
            variations[syst_name]["up"] = np.ones(len(weights._weight))
            variations[syst_name]["down"] = np.ones(len(weights._weight))

        for index in range(len(channel)):
            pnumber = index + 1
            sf = np.ones(len(weights._weight))

            if channel[index] == "t":
                _sf = evaluator.evaluate(
                    pair[f'obj_{pnumber}'].pt, pair[f'obj_{pnumber}'].decayMode, pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, "nom", "dm"
                )
                for syst_name,json_syst_name in systematics_map.items():
                    for updown in ["up", "down"]:
                        json_variation = f"{json_syst_name}_{updown}".replace("$DM", str(pair[f'obj_{pnumber}'].decayMode)).replace("$ERA", year)
                        sf_updown = evaluator.evaluate(
                            pair[f'obj_{pnumber}'].pt, pair[f'obj_{pnumber}'].decayMode, pair[f'obj_{pnumber}'].genPartFlav,wp_VSjet, wp_VSe, json_variation, "dm"
                        )

                        variations[f"{syst_name}_{pnumber}"][updown] *= (sf_updown / _sf)

        sfs_up = [variations[syst_name]["up"] for syst_name in all_variations]
        sfs_down = [variations[syst_name]["down"] for syst_name in all_variations]

    weights.add_multivariation(
        name="Tau_ID",
        weight=sf,
        weightsUp=sfs_up,
        weightsDown=sfs_down,
        modifierNames=all_variations
    )

    return weights


def Electron_ID(pair,channel,weights,year="2017",is_correction=True, **kwargs):

    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL-Electron-ID-SF","2016preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL-Electron-ID-SF","2016postVFP"),
        "Run2_2017": ("2017_UL", "UL-Electron-ID-SF","2017"),
        "Run2_2018": ("2018_UL", "UL-Electron-ID-SF","2018"),
        "Run3_2022": ("2022_Summer22", "Electron-ID-SF","2022Re-recoBCD"),
        "Run3_2022EE": ("2022_Summer22EE", "Electron-ID-SF","2022Re-recoE+PromptFG"),
    }

    folder_name, json_name, year_arg = year_mapping.get(year, (None, None, None))
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/EGamma/{folder_name}/electron.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for electron ID.")

    # no iso is not available at the moment get this to just test
    id_WP = "wp90iso"

    if is_correction:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

        if channel == "et" or channel == "em":
            sf *= evaluator.evaluate(
                year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt
            )

    else:
        sf = np.ones(len(weights._weight))
        _sf = np.ones(len(weights._weight))
        sfup = np.ones(len(weights._weight))
        sfdown = np.ones(len(weights._weight))

        if channel == "et" or channel == "em":
            _sf *= evaluator.evaluate(
                year_arg, "sf", id_WP, pair['obj_1'].eta, pair['obj_1'].pt
            )
            sfup *= evaluator.evaluate(
                year_arg, "sfup", id_WP, pair['obj_1'].eta, pair['obj_1'].pt,
            )
            sfdown *= evaluator.evaluate(
                year_arg, "sfdown", id_WP, pair['obj_1'].eta, pair['obj_1'].pt,
            )

            sfup = sfup
            sfdown = sfdown

    weights.add(name="Electron_ID_", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Electron_Reco(pair,channel,weights,year="2017",is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL", "UL-Electron-ID-SF","2016preVFP"),
        "Run2_2016": ("2016postVFP_UL", "UL-Electron-ID-SF","2016postVFP"),
        "Run2_2017": ("2017_UL", "UL-Electron-ID-SF","2017"),
        "Run2_2018": ("2018_UL", "UL-Electron-ID-SF","2018"),
        "Run3_2022": ("2022_Summer22", "Electron-ID-SF","2022Re-recoBCD"),
        "Run3_2022EE": ("2022_Summer22EE", "Electron-ID-SF","2022Re-recoE+PromptFG"),
    }

    folder_name, json_name, year_arg = year_mapping.get(year, (None, None, None))
    if folder_name and json_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/EGamma/{folder_name}/electron.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for electron reco.")

    low_electron_pt_1 = np.clip(ak.to_numpy(pair['obj_1'].pt), 10.0, 19.99999)
    if "Run3" in year:
        med_electron_pt_1 = np.clip(ak.to_numpy(pair['obj_1'].pt), 20.0, 74.99999)
        high_electron_pt_1 = np.clip(ak.to_numpy(pair['obj_1'].pt), 75.0, np.inf)
    else:
        high_electron_pt_1 = np.clip(ak.to_numpy(pair['obj_1'].pt), 20.0, np.inf)

    sf = np.ones(len(weights._weight))
    sfup, sfdown = None, None

    if channel == "et" or channel == "em":
        if "Run3" not in year:
            sf_low = evaluator.evaluate(
                year_arg, "sf", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
            )
            sf_high = evaluator.evaluate(
                year_arg, "sf", "RecoAbove20", pair['obj_1'].eta, high_electron_pt_1
            )

            sf_low = ak.to_numpy(np.where(pair['obj_1'].pt < 20.0, sf_low, np.ones(len(weights._weight))))
            sf_high = ak.to_numpy(np.where(pair['obj_1'].pt >= 20.0, sf_high, np.ones(len(weights._weight))))
            sf *= (sf_low * sf_high)

        else:
            sf_low = evaluator.evaluate(
                year_arg, "sf", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
            )
            sf_med = evaluator.evaluate(
                year_arg, "sf", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1
            )
            sf_high = evaluator.evaluate(
                year_arg, "sf", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1
            )

            sf_low = ak.to_numpy(np.where(pair['obj_1'].pt < 20.0, sf_low, np.ones(len(weights._weight))))
            sf_med = ak.to_numpy(np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med, np.ones(len(weights._weight))))
            sf_high = ak.to_numpy(np.where(pair['obj_1'].pt >= 75.0, sf_high, np.ones(len(weights._weight))))

            sf *= (sf_low * sf_med * sf_high)

        if not is_correction:
            if "Run3" not in year:
                sf_low_up = evaluator.evaluate(
                    year_arg, "sfup", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                )
                sf_low_down = evaluator.evaluate(
                    year_arg, "sfdown", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                )
                sf_high_up = evaluator.evaluate(
                    year_arg, "sfup", "RecoAbove20", pair['obj_1'].eta, high_electron_pt_1
                )
                sf_high_down = evaluator.evaluate(
                    year_arg, "sfdown", "RecoAbove20", pair['obj_1'].eta, high_electron_pt_1
                )
                sf_low_up = np.where(pair['obj_1'].pt < 20.0, sf_low_up, np.ones(len(weights._weight)))
                sf_low_down = np.where(pair['obj_1'].pt < 20.0, sf_low_down, np.ones(len(weights._weight)))
                sf_high_up = np.where(pair['obj_1'].pt >= 20.0, sf_high_up, np.ones(len(weights._weight)))
                sf_high_down = np.where(pair['obj_1'].pt >= 20.0, sf_high_down, np.ones(len(weights._weight)))

                sfup = sf_low_up * sf_high_up
                sfdown = sf_low_down * sf_high_down
            else:
                sf_low_up = evaluator.evaluate(
                    year_arg, "sfup", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                )
                sf_low_down = evaluator.evaluate(
                    year_arg, "sfdown", "RecoBelow20", pair['obj_1'].eta, low_electron_pt_1
                )
                sf_med_up = evaluator.evaluate(
                    year_arg, "sfup", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1
                )
                sf_med_down = evaluator.evaluate(
                    year_arg, "sfdown", "Reco20to75", pair['obj_1'].eta, med_electron_pt_1
                )
                sf_high_up = evaluator.evaluate(
                    year_arg, "sfup", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1
                )
                sf_high_down = evaluator.evaluate(
                    year_arg, "sfdown", "RecoAbove75", pair['obj_1'].eta, high_electron_pt_1
                )
                sf_low_up = np.where(pair['obj_1'].pt < 20.0, sf_low_up, np.ones(len(weights._weight)))
                sf_low_down = np.where(pair['obj_1'].pt < 20.0, sf_low_down, np.ones(len(weights._weight)))
                sf_med_up = np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med_up, np.ones(len(weights._weight)))
                sf_med_down = np.where((pair['obj_1'].pt >= 20.0) & (pair['obj_1'].pt < 75.0), sf_med_down, np.ones(len(weights._weight)))
                sf_high_up = np.where(pair['obj_1'].pt >= 75.0, sf_high_up, np.ones(len(weights._weight)))
                sf_high_down = np.where(pair['obj_1'].pt >= 75.0, sf_high_down, np.ones(len(weights._weight)))

                sfup = sf_low_up * sf_med_up * sf_high_up
                sfdown = sf_low_down * sf_med_down * sf_high_down

            sf = np.ones(len(weights._weight))

    weights.add(name="Electron_Reco", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Muon_ID(pair,channel,weights,year,is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL"),
        "Run2_2016": ("2016postVFP_UL"),
        "Run2_2017": ("2017_UL"),
        "Run2_2018": ("2018_UL"),
        "Run3_2022": ("2022_Summer22"),
        "Run3_2022EE": ("2022_Summer22EE"),
        "Run3_2023": ("2023_Summer23"),
        "Run3_2023BPix": ("2023_Summer23BPix"),
    }

    folder_name = year_mapping.get(year, (None, None))
    id_WP = "Medium"
    json_name = "NUM_" + id_WP + "ID_DEN_TrackerMuons"

    if folder_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/Muon/{folder_name}/muon_Z.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for muon ID.")

    if is_correction:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

        if channel == "mt":
            sf *= evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
        elif channel == "em":
            sf *= evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
        elif channel == "mm":
            sf_lead = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sf *= (sf_lead * sf_sublead)

    else:
        if channel == "mt":
            sf = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sfup = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systup"
            )
            sfdown = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systdown"
            )

        elif channel == "em":
            sf = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sfup = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systup"
            )
            sfdown = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systdown"
            )

        elif channel == "mm":
            sf_lead_up = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systup"
            )
            sf_sublead_up = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systup"
            )
            sf_lead_down = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systdown"
            )
            sf_sublead_down = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systdown"
            )

            sfup = sf_lead_up * sf_sublead_up
            sfdown = sf_lead_down * sf_sublead_down

        sf = np.ones(len(weights._weight))

    weights.add(name="Muon_ID", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Muon_Isolation(pair,channel,weights,year,is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL"),
        "Run2_2016": ("2016postVFP_UL"),
        "Run2_2017": ("2017_UL"),
        "Run2_2018": ("2018_UL"),
        "Run3_2022": ("2022_Summer22"),
        "Run3_2022EE": ("2022_Summer22EE"),
        "Run3_2023": ("2023_Summer23"),
        "Run3_2023BPix": ("2023_Summer23BPix"),
    }

    folder_name = year_mapping.get(year, (None, None))
    json_name = "NUM_LoosePFIso_DEN_MediumID"

    if folder_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/Muon/{folder_name}/muon_Z.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for muon Isolation.")

    if is_correction:
        sf = np.ones(len(weights._weight))
        sfup, sfdown = None, None

        if channel == "mt":
            sf *= evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
        elif channel == "em":
            sf *= evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
        elif channel == "mm":
            sf_lead = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sf *= (sf_lead * sf_sublead)

    else:
        if channel == "mt":
            sf = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
            )
            sfup = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systup"
            )
            sfdown = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systdown"
            )

        elif channel == "em":
            sf = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
            )
            sfup = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systup"
            )
            sfdown = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systdown"
            )

        elif channel == "mm":
            sf_lead_up = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systup"
            )
            sf_sublead_up = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systup"
            )
            sf_lead_down = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "systdown"
            )
            sf_sublead_down = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "systdown"
            )

            sfup = sf_lead_up * sf_sublead_up
            sfdown = sf_lead_down * sf_sublead_down

        sf = np.ones(len(weights._weight))

    weights.add(name="Muon_Isolation", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Muon_Reco(pair,channel,weights,year,is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL"),
        "Run2_2016": ("2016postVFP_UL"),
        "Run2_2017": ("2017_UL"),
        "Run2_2018": ("2018_UL"),
        "Run3_2022": ("2022_Summer22"),
        "Run3_2022EE": ("2022_Summer22EE"),
        "Run3_2023": ("2023_Summer23"),
        "Run3_2023BPix": ("2023_Summer23BPix"),
    }

    folder_name = year_mapping.get(year, (None, None))
    json_name = "NUM_GlobalMuons_DEN_genTracks"

    if folder_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/Muon/{folder_name}/muon_Z.json.gz")
        if "Run3" not in year:
            evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for muon reco.")

    if is_correction:
        # Only calculate for UL (Not ready for 2022 onwards yet)
        if "Run3" not in year:
            if channel == "mt":
                sf = evaluator.evaluate(
                    np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
                )
            elif channel == "em":
                sf = evaluator.evaluate(
                    np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
                )
            elif channel == "mm":
                sf_lead = evaluator.evaluate(
                    np.abs(pair['obj_1'].eta), pair['obj_1'].pt, "nominal"
                )
                sf_sublead = evaluator.evaluate(
                    np.abs(pair['obj_2'].eta), pair['obj_2'].pt, "nominal"
                )
                sf = sf_lead * sf_sublead
        else:
            sf = np.ones(len(weights._weight))

        sfup, sfdown = None, None

    weights.add(name="Muon_Reco", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def Muon_Trigger(pair,channel,weights,year,is_correction=True, **kwargs):
    # Mapping from year names to folder names and JSON names
    year_mapping = {
        "Run2_2016_HIPM": ("2016preVFP_UL"),
        "Run2_2016": ("2016postVFP_UL"),
        "Run2_2017": ("2017_UL"),
        "Run2_2018": ("2018_UL"),
        "Run3_2022": ("2022_Summer22"),
        "Run3_2022EE": ("2022_Summer22EE"),
        "Run3_2023": ("2023_Summer23"),
        "Run3_2023BPix": ("2023_Summer23BPix"),
    }

    folder_name = year_mapping.get(year, (None, None))
    Muon_WP = "Medium"
    json_name = f"NUM_IsoMu24_DEN_CutBasedId{Muon_WP}_and_PFIsoMedium"

    if folder_name:
        path_to_json = os.path.join(os.path.dirname(__file__), f"JSONs/Muon/{folder_name}/muon_Z.json.gz")
        evaluator = correctionlib.CorrectionSet.from_file(path_to_json)[json_name]
    else:
        raise ValueError(f"Year {year} not supported for muon trigger.")

    pt_1_clipped = np.clip(
        ak.to_numpy(pair['obj_1'].pt),
        26.0,
        np.inf
    )
    pt_2_clipped = np.clip(
        ak.to_numpy(pair['obj_2'].pt),
        26.0,
        np.inf
    )

    if is_correction:
        if channel == "mt":
            sf = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pt_1_clipped, "nominal"
            )
            sf = np.where(
                pair['obj_1'].pt >= 26.0,
                sf,
                np.ones(len(weights._weight))
            )
        elif channel == "em":
            sf = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pt_2_clipped, "nominal"
            )
            sf = np.where(
                pair['obj_2'].pt >= 26.0,
                sf,
                np.ones(len(weights._weight))
            )
        elif channel == "mm":
            sf_lead = evaluator.evaluate(
                np.abs(pair['obj_1'].eta), pt_1_clipped, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                np.abs(pair['obj_2'].eta), pt_2_clipped, "nominal"
            )
            sf_lead = np.where(
                pair['obj_1'].pt >= 26.0,
                sf_lead,
                np.ones(len(weights._weight))
            )
            sf_sublead = np.where(
                pair['obj_2'].pt >= 26.0,
                sf_sublead,
                np.ones(len(weights._weight))
            )
            # sf = sf_lead * sf_sublead
            sf = sf_lead
        else:
            sf = np.ones(len(weights._weight))

        sfup, sfdown = None, None

    weights.add(name="Muon_Trigger", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def SF_photon_ID(
    photons, weights, year="2017", WP="Loose", is_correction=True, **kwargs
):
    """
    Applies the photon ID scale-factor and corresponding uncertainties for the customised cut on the EGamma MVA ID (Run 3)
    JLS removed the support for the EGamma MVA ID SFs from https://gitlab.cern.ch/cms-nanoAOD/jsonpog-integration for Run 2 for now as this is not commonly used in the Hgg group
    Take action yourself or contact us if you need those!
    """
    # era/year defined as parameter of the function
    avail_years = ["2022preEE", "2022postEE"]
    if year not in avail_years:
        print(f"\n WARNING: only scale corrections for the year strings {avail_years} are already implemented! \n Exiting. \n")
        print("If you need the SFs for the central Egamma MVA ID for Run 2 UL, take action yourself or contact us!")
        exit()

    if year == "2022preEE":
        json_file = os.path.join(os.path.dirname(__file__), "JSONs/SF_photon_ID/2022/PhotonIDMVA_2022PreEE.json")
    elif year == "2022postEE":
        json_file = os.path.join(os.path.dirname(__file__), "JSONs/SF_photon_ID/2022/PhotonIDMVA_2022PostEE.json")

    evaluator = correctionlib.CorrectionSet.from_file(json_file)["PhotonIDMVA_SF"]

    # In principle, we should use the fully correct formula https://indico.cern.ch/event/1360948/contributions/5783762/attachments/2788516/4870824/24_02_02_HIG-23-014_PreAppPres.pdf#page=7
    # However, if the SF is pt-binned, the approximation of the multiplication of the two SFs is fully exact
    if "2022" in year:
        if is_correction:
            # only calculate correction to nominal weight
            sf_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].pt, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].pt, "nominal"
            )
            sf = sf_lead * sf_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs

            sf = np.ones(len(weights._weight))
            sf_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].pt, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].pt, "nominal"
            )
            _sf = sf_lead * sf_sublead

            sf_unc_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].pt, "uncertainty"
            )
            sf_unc_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].pt, "uncertainty"
            )

            sfup = (sf_lead + sf_unc_lead) * (sf_sublead + sf_unc_sublead) / _sf

            sfdown = (sf_lead - sf_unc_lead) * (sf_sublead - sf_unc_sublead) / _sf

    weights.add(name="SF_photon_ID", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def LooseMvaSF(photons, weights, year="2017", is_correction=True, **kwargs):
    """
    LooseMvaSF: correction to the event weight on a per photon level, impacting one of the high importance input variable of the DiphotonBDT, binned in eta and r9.
    for original implementation look at: https://github.com/cms-analysis/flashgg/blob/2677dfea2f0f40980993cade55144636656a8a4f/Systematics/python/flashggDiPhotonSystematics2017_Legacy_cfi.py
    And for presentation on the study: https://indico.cern.ch/event/963617/contributions/4103623/attachments/2141570/3608645/Zee_Validation_UL2017_Update_09112020_Prasant.pdf

    2022: up to this point, it applies the 2017 SF with the new formula for combining the SF for the diphoton candidate.
    """

    # era/year defined as parameter of the function, only 2017 is implemented up to now
    avail_years = ["2016", "2016preVFP", "2016postVFP", "2017", "2018", "2022preEE", "2022postEE"]
    if year not in avail_years:
        print(f"\n WARNING: only LooseMvaSF corrections for the year strings {avail_years} are already implemented! \n Exiting. \n")
        exit()
    elif "2016" in year:
        year = "2016"

    # make this read the 2022 files when available!
    # 2017 file should be renamed with the year in its name...
    json_file = os.path.join(os.path.dirname(__file__), f"JSONs/LooseMvaSF/{year}/LooseMvaSF_{year}.json")
    evaluator = correctionlib.CorrectionSet.from_file(json_file)["LooseMvaSF"]
    if year in ["2016", "2017", "2018"]:
        if is_correction:
            # only calculate correction to nominal weight
            sf_lead = evaluator.evaluate(
                "nominal", photons["pho_lead"].ScEta, photons["pho_lead"].r9
            )
            sf_sublead = evaluator.evaluate(
                "nominal", photons["pho_sublead"].ScEta, photons["pho_sublead"].r9
            )
            sf = sf_lead * sf_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs
            sf = np.ones(len(weights._weight))
            sf_lead = evaluator.evaluate(
                "nominal", photons["pho_lead"].ScEta, photons["pho_lead"].r9
            )
            sf_sublead = evaluator.evaluate(
                "nominal", photons["pho_sublead"].ScEta, photons["pho_sublead"].r9
            )
            _sf = sf_lead * sf_sublead

            sfup_lead = evaluator.evaluate(
                "up", photons["pho_lead"].ScEta, photons["pho_lead"].r9
            )
            sfup_sublead = evaluator.evaluate(
                "up", photons["pho_sublead"].ScEta, photons["pho_sublead"].r9
            )
            sfup = sfup_lead * sfup_sublead / _sf

            sfdown_lead = evaluator.evaluate(
                "down", photons["pho_lead"].ScEta, photons["pho_lead"].r9
            )
            sfdown_sublead = evaluator.evaluate(
                "down", photons["pho_sublead"].ScEta, photons["pho_sublead"].r9
            )
            sfdown = sfdown_lead * sfdown_sublead / _sf

    elif "2022" in year:

        if is_correction:
            # only calculate correction to nominal weight
            # ToDo: include pT!!!
            sf_lead_p_lead = evaluator.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9  # photons["pho_lead"].pt
            )
            sf_lead_p_sublead = evaluator.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9  # photons["pho_sublead"].pt
            )
            sf_sublead_p_lead = evaluator.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9  # photons["pho_lead"].pt
            )
            sf_sublead_p_sublead = evaluator.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9  # photons["pho_sublead"].pt
            )
            sf = sf_lead_p_lead * sf_sublead_p_sublead + sf_lead_p_sublead * sf_sublead_p_lead - sf_lead_p_lead * sf_lead_p_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs
            sf = np.ones(len(weights._weight))

            # get nominal SF to divide it out
            sf_lead_p_lead = evaluator.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9  # photons["pho_lead"].pt
            )
            sf_lead_p_sublead = evaluator.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9  # photons["pho_sublead"].pt
            )
            sf_sublead_p_lead = evaluator.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9  # photons["pho_lead"].pt
            )
            sf_sublead_p_sublead = evaluator.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9  # photons["pho_sublead"].pt
            )
            _sf = sf_lead_p_lead * sf_sublead_p_sublead + sf_lead_p_sublead * sf_sublead_p_lead - sf_lead_p_lead * sf_lead_p_sublead

            # up SF
            sfup_lead_p_lead = evaluator.evaluate(
                "up", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9  # photons["pho_lead"].pt
            )
            sfup_lead_p_sublead = evaluator.evaluate(
                "up", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9  # photons["pho_sublead"].pt
            )
            sfup_sublead_p_lead = evaluator.evaluate(
                "up", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9  # photons["pho_lead"].pt
            )
            sfup_sublead_p_sublead = evaluator.evaluate(
                "up", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9  # photons["pho_sublead"].pt
            )
            sfup = (sfup_lead_p_lead * sfup_sublead_p_sublead + sfup_lead_p_sublead * sfup_sublead_p_lead - sfup_lead_p_lead * sfup_lead_p_sublead) / _sf

            # down SF
            sfdown_lead_p_lead = evaluator.evaluate(
                "down", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9  # photons["pho_lead"].pt
            )
            sfdown_lead_p_sublead = evaluator.evaluate(
                "down", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9  # photons["pho_sublead"].pt
            )
            sfdown_sublead_p_lead = evaluator.evaluate(
                "down", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9  # photons["pho_lead"].pt
            )
            sfdown_sublead_p_sublead = evaluator.evaluate(
                "down", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9  # photons["pho_sublead"].pt
            )
            sfdown = (sfdown_lead_p_lead * sfdown_sublead_p_sublead + sfdown_lead_p_sublead * sfdown_sublead_p_lead - sfdown_lead_p_lead * sfdown_lead_p_sublead) / _sf

    weights.add(name="LooseMvaSF", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def ElectronVetoSF(photons, weights, year="2017", is_correction=True, **kwargs):
    """
    ElectronVetoSF: correction to the event weight on a per photon level, Conversion safe veto efficiency with event counting method: To check if the FSR photons are passing the e-veto or not.
    binned in abs(SCeta) and r9.
    for original implementation look at: https://github.com/cms-analysis/flashgg/blob/2677dfea2f0f40980993cade55144636656a8a4f/Systematics/python/flashggDiPhotonSystematics2017_Legacy_cfi.py
    And for presentation on the study: https://indico.cern.ch/event/961164/contributions/4089584/attachments/2135019/3596299/Zmmg_UL2017%20With%20CorrMC_Hgg%20(02.11.2020).pdf
    """

    # era/year defined as parameter of the function
    avail_years = ["2016", "2016preVFP", "2016postVFP", "2017", "2018", "2022preEE", "2022postEE"]
    if year not in avail_years:
        print(f"\n WARNING: only eVetoSF corrections for the year strings {avail_years} are already implemented! \n Exiting. \n")
        exit()
    elif "2016" in year:
        year = "2016"

    if year in ["2016", "2017", "2018"]:
        # 2017 file should be renamed with the year in its name...
        json_file = os.path.join(os.path.dirname(__file__), f"JSONs/ElectronVetoSF/{year}/eVetoSF_{year}.json")
        evaluator = correctionlib.CorrectionSet.from_file(json_file)["ElectronVetoSF"]
        if is_correction:
            # only calculate correction to nominal weight
            sf_lead = evaluator.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9
            )
            sf_sublead = evaluator.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9
            )
            sf = sf_lead * sf_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs
            sf = np.ones(len(weights._weight))
            sf_lead = evaluator.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9
            )
            sf_sublead = evaluator.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9
            )
            _sf = sf_lead * sf_sublead

            sfup_lead = evaluator.evaluate(
                "up", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9
            )
            sfup_sublead = evaluator.evaluate(
                "up", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9
            )
            sfup = sfup_lead * sfup_sublead / _sf

            sfdown_lead = evaluator.evaluate(
                "down", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9
            )
            sfdown_sublead = evaluator.evaluate(
                "down", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9
            )
            sfdown = sfdown_lead * sfdown_sublead / _sf

    elif "2022" in year:
        # presentation of SF: https://indico.cern.ch/event/1360961/#173-run-3-electron-veto-sfs
        if year == "2022preEE":
            json_file = os.path.join(os.path.dirname(__file__), "JSONs/ElectronVetoSF/2022/preEE_CSEV_SFcorrections.json")
        if year == "2022postEE":
            json_file = os.path.join(os.path.dirname(__file__), "JSONs/ElectronVetoSF/2022/postEE_CSEV_SFcorrections.json")
        evaluator = correctionlib.CorrectionSet.from_file(json_file)["CSEV_SFs"]

        if is_correction:
            # only calculate correction to nominal weight
            sf_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, "nominal"
            )
            sf = sf_lead * sf_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs
            sf = np.ones(len(weights._weight))
            sf_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, "nominal"
            )
            _sf = sf_lead * sf_sublead

            unc_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, "uncertainty"
            )
            unc_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, "uncertainty"
            )

            sfup = (sf_lead + unc_lead) * (sf_sublead + unc_sublead) / _sf
            sfdown = (sf_lead - unc_lead) * (sf_sublead - unc_sublead) / _sf

    weights.add(name="ElectronVetoSF", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def PreselSF(photons, weights, year="2017", is_correction=True, **kwargs):
    """
    Preselection SF: correction to the event weight on a per photon level for UL2017. Dt:17/11/2020
    Binned in abs(SCeta) and r9.
    For original implementation look at: https://github.com/cms-analysis/flashgg/blob/2677dfea2f0f40980993cade55144636656a8a4f/Systematics/python/flashggDiPhotonSystematics2017_Legacy_cfi.py
    Link to the Presentation: https://indico.cern.ch/event/963617/contributions/4103623/attachments/2141570/3608645/Zee_Validation_UL2017_Update_09112020_Prasant.pdf
    """

    # era/year defined as parameter of the function
    avail_years = ["2016", "2016preVFP", "2016postVFP", "2017", "2018", "2022preEE", "2022postEE"]
    if year not in avail_years:
        print(f"\n WARNING: only PreselSF corrections for the year strings {avail_years} are already implemented! \n Exiting. \n")
        exit()
    elif "2016" in year:
        year = "2016"

    if year in ["2016", "2017", "2018"]:
        json_file = os.path.join(os.path.dirname(__file__), f"JSONs/Preselection/{year}/PreselSF_{year}.json")
    elif year == "2022preEE":
        json_file = os.path.join(os.path.dirname(__file__), "JSONs/Preselection/2022/Preselection_2022PreEE.json")
    elif year == "2022postEE":
        json_file = os.path.join(os.path.dirname(__file__), "JSONs/Preselection/2022/Preselection_2022PostEE.json")

    if year in ["2016", "2017", "2018"]:
        evaluator = correctionlib.CorrectionSet.from_file(json_file)["PreselSF"]
    elif "2022" in year:
        evaluator = correctionlib.CorrectionSet.from_file(json_file)["Preselection_SF"]

    if year in ["2016", "2017", "2018"]:
        if is_correction:
            # only calculate correction to nominal weight
            sf_lead = evaluator.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9
            )
            sf_sublead = evaluator.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9
            )
            sf = sf_lead * sf_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs
            sf = np.ones(len(weights._weight))
            sf_lead = evaluator.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9
            )
            sf_sublead = evaluator.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9
            )
            _sf = sf_lead * sf_sublead

            sfup_lead = evaluator.evaluate(
                "up", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9
            )
            sfup_sublead = evaluator.evaluate(
                "up", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9
            )
            sfup = sfup_lead * sfup_sublead / _sf

            sfdown_lead = evaluator.evaluate(
                "down", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9
            )
            sfdown_sublead = evaluator.evaluate(
                "down", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9
            )
            sfdown = sfdown_lead * sfdown_sublead / _sf

    # In principle, we should use the fully correct formula https://indico.cern.ch/event/1360948/contributions/5783762/attachments/2788516/4870824/24_02_02_HIG-23-014_PreAppPres.pdf#page=7
    # However, if the SF is pt-binned, the approximation of the multiplication of the two SFs is fully exact
    elif "2022" in year:
        if is_correction:
            # only calculate correction to nominal weight
            sf_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt, "nominal"
            )
            sf = sf_lead * sf_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs

            # Slightly different calculation compared to 2017
            # In the 2022 JSONs, the delta is saved as the uncertainty, not the up/down variations of (SF+-delta) themselves
            # Note that the uncertainty is assumed to be symmetric

            sf = np.ones(len(weights._weight))
            sf_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt, "nominal"
            )
            sf_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt, "nominal"
            )
            _sf = sf_lead * sf_sublead

            sf_unc_lead = evaluator.evaluate(
                abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt, "uncertainty"
            )
            sf_unc_sublead = evaluator.evaluate(
                abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt, "uncertainty"
            )

            sfup = (sf_lead + sf_unc_lead) * (sf_sublead + sf_unc_sublead) / _sf

            sfdown = (sf_lead - sf_unc_lead) * (sf_sublead - sf_unc_sublead) / _sf

    weights.add(name="PreselSF", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def TriggerSF(photons, weights, year="2017", is_correction=True, **kwargs):
    """
    Trigger SF: for full 2017 legacy  B-F dataset. Trigger scale factors for use without HLT applied in MC
    Binned in abs(SCeta), r9 and pt.
    For original implementation look at: https://github.com/cms-analysis/flashgg/blob/2677dfea2f0f40980993cade55144636656a8a4f/Systematics/python/flashggDiPhotonSystematics2017_Legacy_cfi.py
    """

    # era/year defined as parameter of the function
    avail_years = ["2016", "2016preVFP", "2016postVFP", "2017", "2018", "2022preEE", "2022postEE"]
    if year not in avail_years:
        print(f"\n WARNING: only TriggerSF corrections for the year strings {avail_years} are already implemented! \n Exiting. \n")
        exit()
    elif "2016" in year:
        year = "2016"

    json_file_lead = os.path.join(os.path.dirname(__file__), f"JSONs/TriggerSF/{year}/TriggerSF_lead_{year}.json")
    json_file_sublead = os.path.join(os.path.dirname(__file__), f"JSONs/TriggerSF/{year}/TriggerSF_sublead_{year}.json")
    evaluator_lead = correctionlib.CorrectionSet.from_file(json_file_lead)["TriggerSF"]
    evaluator_sublead = correctionlib.CorrectionSet.from_file(json_file_sublead)["TriggerSF"]

    if year in ["2016", "2017", "2018"]:
        if is_correction:
            # only calculate correction to nominal weight
            sf_lead = evaluator_lead.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
            )
            sf_sublead = evaluator_sublead.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
            )
            sf = sf_lead * sf_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs
            sf = np.ones(len(weights._weight))
            sf_lead = evaluator_lead.evaluate(
                "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
            )
            sf_sublead = evaluator_sublead.evaluate(
                "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
            )
            _sf = sf_lead * sf_sublead

            sfup_lead = evaluator_lead.evaluate(
                "up", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
            )
            sfup_sublead = evaluator_sublead.evaluate(
                "up", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
            )
            sfup = sfup_lead * sfup_sublead / _sf

            sfdown_lead = evaluator_lead.evaluate(
                "down", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
            )
            sfdown_sublead = evaluator_sublead.evaluate(
                "down", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
            )
            sfdown = sfdown_lead * sfdown_sublead / _sf

    elif "2022" in year:

        sf_lead_p_lead = evaluator_lead.evaluate(
            "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
        )
        sf_lead_p_sublead = evaluator_lead.evaluate(
            "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
        )
        sf_sublead_p_lead = evaluator_sublead.evaluate(
            "nominal", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
        )
        sf_sublead_p_sublead = evaluator_sublead.evaluate(
            "nominal", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
        )

        if is_correction:
            # only calculate correction to nominal weight
            sf = sf_lead_p_lead * sf_sublead_p_sublead + sf_lead_p_sublead * sf_sublead_p_lead - sf_lead_p_lead * sf_lead_p_sublead

            sfup, sfdown = None, None

        else:
            # only calculate systs
            sf = np.ones(len(weights._weight))
            # get nominal SF to divide it out
            _sf = sf_lead_p_lead * sf_sublead_p_sublead + sf_lead_p_sublead * sf_sublead_p_lead - sf_lead_p_lead * sf_lead_p_sublead

            # up SF
            sfup_lead_p_lead = evaluator_lead.evaluate(
                "up", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
            )
            sfup_lead_p_sublead = evaluator_lead.evaluate(
                "up", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
            )
            sfup_sublead_p_lead = evaluator_sublead.evaluate(
                "up", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
            )
            sfup_sublead_p_sublead = evaluator_sublead.evaluate(
                "up", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
            )
            sfup = (sfup_lead_p_lead * sfup_sublead_p_sublead + sfup_lead_p_sublead * sfup_sublead_p_lead - sfup_lead_p_lead * sfup_lead_p_sublead) / _sf

            # down SF
            sfdown_lead_p_lead = evaluator_lead.evaluate(
                "down", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
            )
            sfdown_lead_p_sublead = evaluator_lead.evaluate(
                "down", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
            )
            sfdown_sublead_p_lead = evaluator_sublead.evaluate(
                "down", abs(photons["pho_lead"].ScEta), photons["pho_lead"].r9, photons["pho_lead"].pt
            )
            sfdown_sublead_p_sublead = evaluator_sublead.evaluate(
                "down", abs(photons["pho_sublead"].ScEta), photons["pho_sublead"].r9, photons["pho_sublead"].pt
            )
            sfdown = (sfdown_lead_p_lead * sfdown_sublead_p_sublead + sfdown_lead_p_sublead * sfdown_sublead_p_lead - sfdown_lead_p_lead * sfdown_lead_p_sublead) / _sf

    weights.add(name="TriggerSF", weight=sf, weightUp=sfup, weightDown=sfdown)

    return weights


def NNLOPS(
    events, dataset_name, weights, is_correction=True, generator="mcatnlo", **kwargs
):
    """
    --- NNLOPS reweighting for ggH events to be applied to NLO Madgraph (and Powheg).
    Swap generator argument to 'powheg' if to be applied to powheg events
    Reweight event based on truth Higgs pt and number of jets, extracted from HTXS object
    Constructs njet-dependent linear splines based on input data, functions of Higgs pt
    Reweighting is applied always if correction is specified in runner JSON.
    Warning is thrown if ggh or glugluh is not in the name.
    """
    json_file = os.path.join(os.path.dirname(__file__), "JSONs/NNLOPS_reweight.json")

    if is_correction:
        if 'ggh' not in dataset_name.lower() and 'glugluh' not in dataset_name.lower():
            logger.info(f"\n WARNING: You specified NNLOPS reweighting for dataset with {dataset_name} but this does not appear like a ggF sample. Consider checking your runner JSON Proceed with caution.")
        # Extract NNLOPS weights from json file
        with open(json_file, "r") as jf:
            nnlops_reweight = json.load(jf)

        # Load reweight factors for specific generator
        nnlops_reweight = nnlops_reweight[generator]

        # Build linear splines for different njet bins
        spline_0jet = interp1d(
            nnlops_reweight["0jet"]["pt"], nnlops_reweight["0jet"]["weight"]
        )
        spline_1jet = interp1d(
            nnlops_reweight["1jet"]["pt"], nnlops_reweight["1jet"]["weight"]
        )
        spline_2jet = interp1d(
            nnlops_reweight["2jet"]["pt"], nnlops_reweight["2jet"]["weight"]
        )
        spline_ge3jet = interp1d(
            nnlops_reweight["3jet"]["pt"], nnlops_reweight["3jet"]["weight"]
        )

        # Load truth Higgs pt and njets (pt>30) from events
        higgs_pt = events.HTXS.Higgs_pt
        njets30 = events.HTXS.njets30

        # Extract scale factors from splines and mask for different jet bins
        # Define maximum pt values as interpolated splines only go up so far
        sf = (
            (njets30 == 0) * spline_0jet(np.minimum(np.array(higgs_pt), 125.0))
            + (njets30 == 1) * spline_1jet(np.minimum(np.array(higgs_pt), 625.0))
            + (njets30 == 2) * spline_2jet(np.minimum(np.array(higgs_pt), 800.0))
            + (njets30 >= 3) * spline_ge3jet(np.minimum(np.array(higgs_pt), 925.0))
        )

    else:
        raise RuntimeError(
            "NNLOPS reweighting is only a flat correction, not a systematic"
        )

    weights.add("NNLOPS", sf, None, None)

    return weights


def AlphaS(photons, events, weights, dataset_name, **kwargs):
    """
    AlphaS weights variations are the last two of the PDF replicas, e.g.,
    https://github.com/cms-sw/cmssw/blob/d37d2797dffc978a78da2fafec3ba480071a0e67/PhysicsTools/NanoAOD/python/genWeightsTable_cfi.py#L10
    https://lhapdfsets.web.cern.ch/current/NNPDF31_nnlo_as_0118_mc_hessian_pdfas/NNPDF31_nnlo_as_0118_mc_hessian_pdfas.info
    """
    systematic = "AlphaS Weight"
    try:
        weights.add(
            name="AlphaS",
            weight=np.ones(len(events)),
            weightUp=events.LHEPdfWeight[:, -1],
            weightDown=events.LHEPdfWeight[:, -2],
        )
    except:
        logger.debug(
            f"No LHEPdf Weights in dataset {dataset_name}, skip systematic: {systematic}"
        )
        return weights

    return weights


def PartonShower(photons, events, weights, dataset_name, **kwargs):
    """
    Parton Shower weights:
    https://github.com/cms-sw/cmssw/blob/caeae4110ddbada1cfdac195404b3c618584e8fb/PhysicsTools/NanoAOD/plugins/GenWeightsTableProducer.cc#L533-L534
    """
    systematic = "PartonShower weight"
    try:
        weights.add(
            name="PS_ISR",
            weight=np.ones(len(events)),
            weightUp=events.PSWeight[:, 0],
            weightDown=events.PSWeight[:, 2],
        )

        weights.add(
            name="PS_FSR",
            weight=np.ones(len(events)),
            weightUp=events.PSWeight[:, 1],
            weightDown=events.PSWeight[:, 3],
        )
    except:
        logger.debug(
            f"No PS Weights in dataset {dataset_name}, skip systematic: {systematic}"
        )
        return weights

    return weights


def bTagShapeSF(events, weights, is_correction=True, year="2017", **kwargs):
    avail_years = ["2016preVFP", "2016postVFP", "2017", "2018", "2022preEE", "2022postEE", "2023preBPix", "2023postBPix"]
    if year not in avail_years:
        print(f"\n WARNING: only scale corrections for the year strings {avail_years} are already implemented! \n Exiting. \n")
        exit()
    btag_systematics = [
        "lf",
        "hf",
        "cferr1",
        "cferr2",
        "lfstats1",
        "lfstats2",
        "hfstats1",
        "hfstats2",
        "jes",
    ]
    inputFilePath = "JSONs/bTagSF/"
    btag_correction_configs = {
        "2016preVFP": {
            "file": os.path.join(
                inputFilePath , "2016preVFP_UL/btagging.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": btag_systematics,
        },
        "2016postVFP": {
            "file": os.path.join(
                inputFilePath , "2016postVFP_UL/btagging.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": btag_systematics,
        },
        "2017": {
            "file": os.path.join(
                inputFilePath , "2017_UL/btagging.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": btag_systematics,
        },
        "2018": {
            "file": os.path.join(
                inputFilePath , "2018_UL/btagging.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": btag_systematics,
        },
        "2022preEE":{
            "file": os.path.join(
                inputFilePath , "2022_Summer22/btagging.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": btag_systematics,
        },
        "2022postEE":{
            "file": os.path.join(
                inputFilePath , "2022_Summer22EE/btagging.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": btag_systematics,
        },
        "2023preBPix":{
            "file": os.path.join(
                inputFilePath , "2023_Summer23/btagging.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": btag_systematics,
        },
        "2023postBPix":{
            "file": os.path.join(
                inputFilePath , "2023_Summer23BPix/btagging.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": btag_systematics,
        },
    }
    jsonpog_file = os.path.join(
        os.path.dirname(__file__), btag_correction_configs[year]["file"]
    )
    evaluator = correctionlib.CorrectionSet.from_file(jsonpog_file)[
        btag_correction_configs[year]["method"]
    ]

    dummy_sf = ak.ones_like(events["event"])

    relevant_jets = events["sel_jets"][
        np.abs(events["sel_jets"].eta) < 2.5
    ]
    # only calculate correction to nominal weight
    # we will evaluate the scale factors relative to all jets to be multiplied
    jet_pt = relevant_jets.pt
    jet_eta = np.abs(relevant_jets.eta)
    jet_hFlav = relevant_jets.hFlav
    jet_btagDeepFlavB = relevant_jets.btagDeepFlavB

    # Convert the jets in one dimension array and store the orignal structure of the ak array in counts
    flat_jet_pt = ak.flatten(jet_pt)
    flat_jet_eta = ak.flatten(jet_eta)
    flat_jet_btagDeepFlavB = ak.flatten(jet_btagDeepFlavB)
    flat_jet_hFlav = ak.flatten(jet_hFlav)

    counts = ak.num(jet_hFlav)

    logger.info("Warning: you have to normalise b-tag weights afterwards so that they do not change the yield!")

    if is_correction:

        _sf = []
        # Evluate the scale factore per jet and unflatten the scale fatores in original structure
        _sf = ak.unflatten(
            evaluator.evaluate(
                "central",
                flat_jet_hFlav,
                flat_jet_eta,
                flat_jet_pt,
                flat_jet_btagDeepFlavB,
            ),
            counts
        )
        # Multiply the scale factore of all jets in a even
        sf = ak.prod(_sf,axis=1)

        sfs_up = [None for _ in btag_systematics]
        sfs_down = [None for _ in btag_systematics]

    else:
        # only calculate correction to nominal weight
        # replace by accessing partial weight!
        _sf = []
        # Evluate the scale factore per jet and unflatten the scale fatores in original structure
        _sf_central = evaluator.evaluate(
            "central",
            flat_jet_hFlav,
            flat_jet_eta,
            flat_jet_pt,
            flat_jet_btagDeepFlavB,
        )
        # Multiply the scale factore of all jets in a even

        sf = ak.values_astype(dummy_sf, np.float)
        sf_central = ak.prod(
            ak.unflatten(_sf_central, counts),
            axis=1
        )

        variations = {}

        # Define a condiation based the jet flavour because the json file are defined for the 4(c),5(b),0(lf) flavour jets
        flavour_condition = np.logical_or(jet_hFlav < 4,jet_hFlav > 5)
        # Replace the flavour to 0 (lf) if the jet flavour is neither 4 nor 5
        jet_hFlav_JSONrestricted = ak.where(flavour_condition, 0 ,jet_hFlav)
        flat_jet_hFlav_JSONrestricted = ak.flatten(jet_hFlav_JSONrestricted)
        # We need a dmmy sf array set to one to multiply for flavour dependent systentic variation
        flat_dummy_sf = ak.ones_like(flat_jet_hFlav_JSONrestricted)

        for syst_name in btag_correction_configs[year]["systs"]:

            # we will append the scale factors relative to all jets to be multiplied
            _sfup = []
            _sfdown = []
            variations[syst_name] = {}

            if "cferr" in syst_name:
                # we to remember which jet is correspond to c(hadron flv 4) jets
                cjet_masks = flat_jet_hFlav_JSONrestricted == 4

                flat_jet_hFlavC_JSONrestricted = ak.where(flat_jet_hFlav_JSONrestricted != 4, 4 ,flat_jet_hFlav_JSONrestricted)
                _Csfup = evaluator.evaluate(
                    "up_" + syst_name,
                    flat_jet_hFlavC_JSONrestricted,
                    flat_jet_eta,
                    flat_jet_pt,
                    flat_jet_btagDeepFlavB,
                )

                _Csfdown = evaluator.evaluate(
                    "down_" + syst_name,
                    flat_jet_hFlavC_JSONrestricted,
                    flat_jet_eta,
                    flat_jet_pt,
                    flat_jet_btagDeepFlavB,
                )
                _Csfup = ak.where(
                    cjet_masks,
                    _Csfup,
                    flat_dummy_sf,
                )
                _Csfdown = ak.where(
                    cjet_masks,
                    _Csfdown,
                    flat_dummy_sf,
                )
                # Replace all the calculated sf with 1 when there is light jet or with flavour b otherwise keep the cerntral weight
                _sfcentral_Masked_notC = ak.where(
                    ~cjet_masks,
                    _sf_central,
                    flat_dummy_sf,
                )
                _sfup = ak.unflatten(np.multiply(_sfcentral_Masked_notC,_Csfup),counts)
                _sfdown = ak.unflatten(np.multiply(_sfcentral_Masked_notC,_Csfdown),counts)
            else:
                # We to remember which jet is correspond to c(hadron flv 4) jets
                cjet_masks = flat_jet_hFlav_JSONrestricted == 4

                flat_jet_hFlavNonC_JSONrestricted = ak.where(cjet_masks, 0 ,flat_jet_hFlav_JSONrestricted)

                _NonCsfup = evaluator.evaluate(
                    "up_" + syst_name,
                    flat_jet_hFlavNonC_JSONrestricted,
                    flat_jet_eta,
                    flat_jet_pt,
                    flat_jet_btagDeepFlavB,
                )

                _NonCsfdown = evaluator.evaluate(
                    "down_" + syst_name,
                    flat_jet_hFlavNonC_JSONrestricted,
                    flat_jet_eta,
                    flat_jet_pt,
                    flat_jet_btagDeepFlavB,
                )

                _NonCsfup = ak.where(
                    ~cjet_masks,
                    _NonCsfup,
                    flat_dummy_sf,
                )
                _NonCsfdown = ak.where(
                    ~cjet_masks,
                    _NonCsfdown,
                    flat_dummy_sf,
                )
                # Replace all the calculated sf with 1 when there is c jet otherwise keep the cerntral weight
                _sfcentral_Masked_C = ak.where(
                    cjet_masks,
                    _sf_central,
                    flat_dummy_sf,
                )
                _sfup = ak.unflatten(np.multiply(_sfcentral_Masked_C,_NonCsfup),counts)
                _sfdown = ak.unflatten(np.multiply(_sfcentral_Masked_C,_NonCsfdown),counts)

            sf_up = ak.prod(_sfup,axis=1)
            sf_down = ak.prod(_sfdown,axis=1)
            variations[syst_name]["up"] = sf_up
            variations[syst_name]["down"] = sf_down
        # coffea weights.add_multivariation() wants a list of arrays for the multiple up and down variations
        # we devide sf_central because cofea processor save the up and down vartion by multiplying the central weights
        sfs_up = [variations[syst_name]["up"] / sf_central for syst_name in btag_systematics]
        sfs_down = [variations[syst_name]["down"] / sf_central for syst_name in btag_systematics]

    weights.add_multivariation(
        name="bTagSF",
        weight=sf,
        modifierNames=btag_systematics,
        weightsUp=sfs_up,
        weightsDown=sfs_down,
        shift=False,
    )

    return weights


def cTagSF(events, weights, is_correction=True, year="2017", **kwargs):
    """
    Add c-tagging reshaping SFs as from /https://github.com/higgs-charm/flashgg/blob/dev/cH_UL_Run2_withBDT/Systematics/scripts/applyCTagCorrections.py
    BTV scale factor Wiki: https://btv-wiki.docs.cern.ch/ScaleFactors/
    events must contain jet objects, moreover evaluation of SFs works by calculating the scale factors for all the jets in the event,
    to do this in columnar style the only thing I could think of was to pad the jet collection to the max(n_jets) keep track of the "fake jets" introduced
    by this procedure and fill these position wit 1s before actually setting the weights in the collection. If someone has better ideas I'm open for suggestions
    """

    # era/year defined as parameter of the function, only Run2 is implemented up to now
    avail_years = ["2016preVFP", "2016postVFP", "2017", "2018"]
    if year not in avail_years:
        print(f"\n WARNING: only scale corrections for the year strings {avail_years} are already implemented! \n Exiting. \n")
        exit()

    ctag_systematics = [
        "Extrap",
        "Interp",
        "LHEScaleWeight_muF",
        "LHEScaleWeight_muR",
        "PSWeightFSR",
        "PSWeightISR",
        "PUWeight",
        "Stat",
        "XSec_BRUnc_DYJets_b",
        "XSec_BRUnc_DYJets_c",
        "XSec_BRUnc_WJets_c",
        "jer",
        "jesTotal",
    ]

    ctag_correction_configs = {
        "2016preVFP": {
            "file": os.path.join(
                os.path.dirname(__file__), "JSONs/cTagSF/2016/ctagging_2016preVFP.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": ctag_systematics,
        },
        "2016postVFP": {
            "file": os.path.join(
                os.path.dirname(__file__), "JSONs/cTagSF/2016/ctagging_2016postVFP.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": ctag_systematics,
        },
        "2017": {
            "file": os.path.join(
                os.path.dirname(__file__), "JSONs/cTagSF/2017/ctagging_2017.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": ctag_systematics,
        },
        "2018": {
            "file": os.path.join(
                os.path.dirname(__file__), "JSONs/cTagSF/2018/ctagging_2018.json.gz"
            ),
            "method": "deepJet_shape",
            "systs": ctag_systematics,
        },
    }

    jsonpog_file = os.path.join(
        os.path.dirname(__file__), ctag_correction_configs[year]["file"]
    )
    evaluator = correctionlib.CorrectionSet.from_file(jsonpog_file)[
        ctag_correction_configs[year]["method"]
    ]

    events["n_jets"] = ak.num(events["sel_jets"])
    max_n_jet = max(events["n_jets"])

    dummy_sf = ak.ones_like(events["event"])

    if is_correction:
        # only calculate correction to nominal weight
        # we will append the scale factors relative to all jets to be multiplied
        _sf = []
        # we need a seres of masks to remember where there were no jets
        masks = []
        # to calculate the SFs we have to distinguish for different number of jets
        for i in range(max_n_jet):
            masks.append(events["n_jets"] > i)

            # I select the nth jet column
            nth_jet_hFlav = choose_jet(events["sel_jets"].hFlav, i, 0)
            nth_jet_DeepFlavour_CvsL = choose_jet(
                events["sel_jets"].btagDeepFlav_CvL, i, 0
            )
            nth_jet_DeepFlavour_CvsB = choose_jet(
                events["sel_jets"].btagDeepFlav_CvB, i, 0
            )
            _sf.append(
                evaluator.evaluate(
                    "central",
                    nth_jet_hFlav,
                    nth_jet_DeepFlavour_CvsL,
                    nth_jet_DeepFlavour_CvsB,
                )
            )

            # and fill the places where we had dummies with ones
            _sf[i] = ak.where(
                masks[i],
                _sf[i],
                dummy_sf,
            )

        sfup, sfdown = None, None
        # here we multiply all the sf for different jets in the event
        sf = dummy_sf
        for nth in _sf:
            sf = sf * nth

        sfs_up = [ak.values_astype(dummy_sf, np.float) for _ in ctag_systematics]
        sfs_down = [ak.values_astype(dummy_sf, np.float) for _ in ctag_systematics]

        weights.add_multivariation(
            name="cTagSF",
            weight=sf,
            modifierNames=ctag_systematics,
            weightsUp=sfs_up,
            weightsDown=sfs_down,
        )

    else:
        # only calculate correction to nominal weight
        # we will append the scale factors relative to all jets to be multiplied
        _sf = []
        # we need a seres of masks to remember where there were no jets
        masks = []
        # to calculate the SFs we have to distinguish for different number of jets
        for i in range(max_n_jet):
            masks.append(events["n_jets"] > i)

            # I select the nth jet column
            nth_jet_hFlav = choose_jet(events["sel_jets"].hFlav, i, 0)
            nth_jet_DeepFlavour_CvsL = choose_jet(
                events["sel_jets"].btagDeepFlav_CvL, i, 0
            )
            nth_jet_DeepFlavour_CvsB = choose_jet(
                events["sel_jets"].btagDeepFlav_CvB, i, 0
            )
            _sf.append(
                evaluator.evaluate(
                    "central",
                    nth_jet_hFlav,
                    nth_jet_DeepFlavour_CvsL,
                    nth_jet_DeepFlavour_CvsB,
                )
            )

            # and fill the places where we had dummies with ones
            _sf[i] = ak.where(
                masks[i],
                _sf[i],
                dummy_sf,
            )

        # here we multiply all the sf for different jets in the event
        sf = dummy_sf
        for nth in _sf:
            sf = sf * nth

        variations = {}
        for syst_name in ctag_correction_configs[year]["systs"]:
            # we will append the scale factors relative to all jets to be multiplied
            _sfup = []
            _sfdown = []
            variations[syst_name] = {}
            for i in range(max_n_jet):
                # I select the nth jet column
                nth_jet_hFlav = choose_jet(events["sel_jets"].hFlav, i, 0)
                nth_jet_DeepFlavour_CvsL = choose_jet(
                    events["sel_jets"].btagDeepFlav_CvL, i, 0
                )
                nth_jet_DeepFlavour_CvsB = choose_jet(
                    events["sel_jets"].btagDeepFlav_CvB, i, 0
                )

                _sfup.append(
                    evaluator.evaluate(
                        "up_" + syst_name,
                        nth_jet_hFlav,
                        nth_jet_DeepFlavour_CvsL,
                        nth_jet_DeepFlavour_CvsB,
                    )
                )

                _sfdown.append(
                    evaluator.evaluate(
                        "down_" + syst_name,
                        nth_jet_hFlav,
                        nth_jet_DeepFlavour_CvsL,
                        nth_jet_DeepFlavour_CvsB,
                    )
                )

                # and fill the places where we had dummies with ones
                _sfup[i] = ak.where(
                    masks[i],
                    _sfup[i],
                    dummy_sf,
                )
                _sfdown[i] = ak.where(
                    masks[i],
                    _sfdown[i],
                    dummy_sf,
                )
            # here we multiply all the sf for different jets in the event
            sfup = dummy_sf
            sfdown = dummy_sf
            for i in range(len(_sf)):
                sfup = sfup * _sfup[i]
                sfdown = sfdown * _sfdown[i]

            variations[syst_name]["up"] = sfup
            variations[syst_name]["down"] = sfdown

        # coffea weights.add_multivariation() wants a list of arrays for the multiple up and down variations
        sfs_up = [variations[syst_name]["up"] / sf for syst_name in ctag_systematics]
        sfs_down = [
            variations[syst_name]["down"] / sf for syst_name in ctag_systematics
        ]

        weights.add_multivariation(
            name="cTagSF",
            weight=dummy_sf,
            modifierNames=ctag_systematics,
            weightsUp=sfs_up,
            weightsDown=sfs_down,
            shift=False,
        )

    return weights


def Zpt(
    events,
    weights,
    logger,
    dataset_name,
    is_correction=True,
    year="2022postEE",
    **kwargs,
):
    """
    Z pt reweighting
    """
    systematic = "Z pt reweighting"

    json_dict = {
        "2016postVFP_UL": os.path.join(
            os.path.dirname(__file__),
            "./JSONs/my_Zpt_reweighting.json.gz",
        ),
        "2016preVFP_UL": os.path.join(
            os.path.dirname(__file__),
            "./JSONs/my_Zpt_reweighting.json.gz",
        ),
        "2017_UL": os.path.join(
            os.path.dirname(__file__),
            "./JSONs/my_Zpt_reweighting.json.gz",
        ),
        "2018_UL": os.path.join(
            os.path.dirname(__file__),
            "./JSONs/my_Zpt_reweighting.json.gz",
        ),
        "2022postEE": os.path.join(
            os.path.dirname(__file__),
            "./JSONs/my_Zpt_reweighting.json.gz",
        ),
        "2023": os.path.join(
            os.path.dirname(__file__),
            "./JSONs/my_Zpt_reweighting.json.gz",
        ),
    }
    key_map = {
        "2016postVFP_UL": "Zpt_reweight",
        "2016preVFP_UL": "Zpt_reweight",
        "2017_UL": "Zpt_reweight",
        "2018_UL": "Zpt_reweight",
        "2022postEE": "Zpt_reweight",
        "2023": "Zpt_reweight",
    }

    # inputs
    input_value = {
        "Zpt": events.mmy_pt,
    }
    cset = correctionlib.CorrectionSet.from_file(json_dict[year])
    # list(cset) # get keys in cset
    sf = cset[key_map[year]]

    logger.debug(f"{systematic}:{key_map[year]}, year: {year} ===> {dataset_name}")
    if is_correction:
        nom = sf.evaluate(input_value["Zpt"])
        weights.add(name="ZptWeight", weight=nom)
    else:
        nom = sf.evaluate(input_value["Zpt"])
        up = sf.evaluate(input_value["Zpt"])
        down = sf.evaluate(input_value["Zpt"])
        weights.add(
            name="ZptWeight",
            weight=ak.ones_like(nom),
            weightUp=up / nom,
            weightDown=down / nom,
        )

    return weights
