import numpy as np
import yaml
import os


def samples_to_stitch(sample):
    samples = [
        'DYto2L_M-50_madgraphMLM',
        'DYto2L_M-50_madgraphMLM_ext1',
        'DYto2L_M-50_1J_madgraphMLM',
        'DYto2L_M-50_2J_madgraphMLM',
        'DYto2L_M-50_3J_madgraphMLM',
        'DYto2L_M-50_4J_madgraphMLM',
        'WtoLNu_madgraphMLM',
        'WtoLNu_madgraphMLM_ext1',
        'WtoLNu_1J_madgraphMLM',
        'WtoLNu_2J_madgraphMLM',
        'WtoLNu_3J_madgraphMLM',
        'WtoLNu_4J_madgraphMLM',
    ]
    if sample in samples:
        if "DY" in sample:
            return (True, "DY")
        else:
            return (True, "WJ")
    return (False, None)


def stitching(year,events,process):
    nLHEjets = events.LHE.Njets
    # load in stitching.yml file to read information from it
    path_file = os.path.join(os.getcwd(), f'scripts/ditau/config/{year}/Stitching.yaml')
    with open(path_file) as f:
        stitching = yaml.safe_load(f)

    # get the stitching information for the given year
    stitching = stitching[process]

    inclusive_xs = stitching['Inclusive']['Cross_section']
    inclusive_eff_evts = stitching['Inclusive']['Effective_Events']

    numerator_1 = (stitching['1J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_2 = (stitching['2J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_3 = (stitching['3J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts
    numerator_4 = (stitching['4J']['Cross_section'] / inclusive_xs) * inclusive_eff_evts

    denominator_1 = numerator_1 + stitching['1J']['Effective_Events']
    denominator_2 = numerator_2 + stitching['2J']['Effective_Events']
    denominator_3 = numerator_3 + stitching['3J']['Effective_Events']
    denominator_4 = numerator_4 + stitching['4J']['Effective_Events']

    # get the weights for the stitching
    weight_1 = numerator_1 / denominator_1
    weight_2 = numerator_2 / denominator_2
    weight_3 = numerator_3 / denominator_3
    weight_4 = numerator_4 / denominator_4

    stitching_weights = np.ones(len(nLHEjets))
    stitching_weights = np.where(
        nLHEjets == 1, weight_1,
        np.where(
            nLHEjets == 2, weight_2,
            np.where(
                nLHEjets == 3, weight_3,
                np.where(nLHEjets == 4, weight_4, stitching_weights)
            )
        )
    )

    return stitching_weights
