from higgs_dna.selections.lumi_selections import select_lumis
from higgs_dna.utils.dumping_utils import (
    dump_ak_array,
    ditau_ak_array
)
from higgs_dna.utils.misc_utils import choose_jet
from higgs_dna.selections.object_selections import delta_r_match_mask, delta_r_mask, delta_r_seed_mask
from higgs_dna.selections.jet_selections import jetvetomap
from higgs_dna.utils.kinematic_utilities import calculate_delta_phi, calculate_transverse_mass, calculate_transverse_momentum, calculate_p4, sum_p4
from higgs_dna.utils.stitching_utils import samples_to_stitch,stitching
from higgs_dna.systematics import object_systematics as available_object_systematics
from higgs_dna.systematics import object_corrections as available_object_corrections
# from higgs_dna.systematics import weight_systematics as available_weight_systematics
from higgs_dna.systematics import weight_corrections as available_weight_corrections

import functools
import operator
import os
from typing import Any, Dict, List, Optional
import awkward
import numpy
import vector
from coffea import processor
from coffea.analysis_tools import Weights

import logging

logger = logging.getLogger(__name__)

vector.register_awkward()


class HtautauBaseProcessor(processor.ProcessorABC):
    def __init__(
        self,
        metaconditions: Dict[str, Any],
        run_effective: bool,
        systematics: Optional[List[Any]],
        corrections: Optional[List[Any]],
        apply_trigger: bool,
        output_location: Optional[str],
        taggers: Optional[List[Any]],
        trigger_group: str,
        analysis: str,
        year: str,
    ) -> None:
        self.meta = metaconditions
        self.run_effective = run_effective
        self.systematics = systematics if systematics is not None else {}
        self.corrections = corrections if corrections is not None else {}
        self.apply_trigger = apply_trigger
        self.output_location = output_location
        self.trigger_group = trigger_group
        self.analysis = analysis
        self.year = year

        self.min_pt_muon = 20.0
        self.min_pt_lead_muon = 25.0

        self.postfixes = {"obj_1": "1", "obj_2": "2"}

    def process_extra(self, events: awkward.Array) -> awkward.Array:
        raise NotImplementedError

    def select_muons(self, muons: awkward.highlevel.Array, muon_pt_threshold: float, muon_max_eta: float) -> awkward.highlevel.Array:
        """
        Selects muons from the input `muons` array based on specified criteria.

        Args:
            muons (awkward.highlevel.Array): The input array containing muon objects.
            muon_pt_threshold (float): The minimum transverse momentum threshold for muon selection.
            muon_max_eta (float): The maximum absolute rapidity for muon selection.

        Returns:
            awkward.highlevel.Array: The selected muons satisfying the given criteria.

        Example:
            muons = ...  # Initialize your awkward.highlevel.Array of muons
            selected_muons = select_muons(muons, muon_pt_threshold=10.0, muon_max_eta=2.4)

        Note:
            - The provided `muons` should be an `awkward.highlevel.Array` containing muon objects.
            - The function applies selection criteria on the `muons` array based on the following conditions:
                - Transverse momentum (pt) greater than `muon_pt_threshold`.
                - Pseudorapidity (eta) within the range (-`muon_max_eta`, +`muon_max_eta`).
                - Medium identification (mediumId) requirement.
                - Particle flow relative isolation (pfRelIso04_all) less than 0.3.
                - Transverse impact parameter (dz) less than 0.2.
                - Longitudinal impact parameter (dxy) less than 0.045.
            - The function returns the muons that satisfy all the specified selection criteria.

        """
        pt_cut = muons.pt > muon_pt_threshold
        eta_cut = abs(muons.eta) < abs(muon_max_eta)
        id_cut = muons.mediumId
        iso_cut = muons.pfRelIso04_all < 0.3
        dz_cut = abs(muons.dz) < 0.2
        dxy_cut = abs(muons.dxy) < 0.045
        return muons[pt_cut & eta_cut & id_cut & iso_cut & dz_cut & dxy_cut]

    def select_electrons(self, electrons: awkward.highlevel.Array, electron_pt_threshold: float, electron_max_eta: float, electron_id : str) -> awkward.highlevel.Array:
        """
        Selects electrons from the input `electrons` array based on specified criteria.

        Args:
            electrons (awkward.highlevel.Array): The input array containing electron objects.
            electron_pt_threshold (float): The minimum transverse momentum threshold for electron selection.
            electron_max_eta (float): The maximum absolute rapidity for electron selection.
            electron_id (str): The name of the electron ID discriminator to use.

        Returns:
            awkward.highlevel.Array: The selected electrons satisfying the given criteria.

        Example:
            electrons = ...  # Initialize your awkward.highlevel.Array of electrons
            selected_electrons = select_electrons(electrons, electron_pt_threshold=10.0, electron_max_eta=2.5, electron_id='mvaNoIso_WP90')
            Recommended IDs are mvaNoIso_WP90 for Run-3 or mvaFall17V1Iso_WP90 for Run-2
        Note:
            - The provided `electrons` should be an `awkward.highlevel.Array` containing electron objects.
            - The function applies selection criteria on the `electrons` array based on the following conditions:
                - Transverse momentum (pt) greater than `electron_pt_threshold`.
                - Pseudorapidity (eta) within the range (-`electron_max_eta`, +`electron_max_eta`).
                - identification requirement -> electron should pass the ID discriminator `electron_id`.
                - Particle flow relative isolation (pfRelIso04_all) less than 0.3.
                - Transverse impact parameter (dz) less than 0.2.
                - Longitudinal impact parameter (dxy) less than 0.045.
            - The function returns the electrons that satisfy all the specified selection criteria.

        """

        pt_cut = electrons.pt > electron_pt_threshold
        eta_cut = abs(electrons.eta) < abs(electron_max_eta)
        id_cut = electrons[electron_id]
        iso_cut = electrons.miniPFRelIso_all < 0.3
        dz_cut = abs(electrons.dz) < 0.2
        dxy_cut = abs(electrons.dxy) < 0.045
        return electrons[pt_cut & eta_cut & id_cut & iso_cut & dz_cut & dxy_cut]

    def select_taus(self, taus: awkward.highlevel.Array, tau_pt_threshold: float, tau_max_eta: float, tau_id : str) -> awkward.highlevel.Array:
        """
        Selects taus from the input `taus` array based on specified criteria.

        Args:
            taus (awkward.highlevel.Array): The input array containing tau objects.
            tau_pt_threshold (float): The minimum transverse momentum threshold for tau selection.
            tau_max_eta (float): The maximum absolute rapidity for tau selection.
            tau_id (str): The name of the tau ID discriminator to use.

        Returns:
            awkward.highlevel.Array: The selected taus satisfying the given criteria.

        Example:
            taus = ...  # Initialize your awkward.highlevel.Array of taus
            selected_taus = select_taus(taus, tau_pt_threshold=18.0, tau_max_eta=2.3, tau_id='DeepTau2018v2p5')

        Note:
            - The provided `taus` should be an `awkward.highlevel.Array` containing tau objects.
            - The function applies selection criteria on the `taus` array based on the following conditions:
                - Transverse momentum (pt) greater than `tau_pt_threshold`.
                - Pseudorapidity (eta) within the range (-`tau_max_eta`, +`tau_max_eta`).
                - identification requirement -> tau should pass the loosest working points of the ID discriminator `tau_id`.
                - Transverse impact parameter (dz) less than 0.2.
            - The function returns the taus that satisfy all the specified selection criteria.

        """
        pt_cut = taus.pt > tau_pt_threshold
        eta_cut = abs(taus.eta) < abs(tau_max_eta)
        # apply the loosest working points for jet and muon discriminators, and the second loosest electron discriminator
        # as we are using the DeepTau ID we also veto dm 5 and 6, when the code is updated to use particleNet we may want to remove this requirement
        id_cut = (taus[f"id{tau_id}VSjet"] > 0) & (taus[f"id{tau_id}VSmu"] > 0) & (taus[f"id{tau_id}VSe"] > 1) & (taus.decayMode != 5) & (taus.decayMode != 6)
        dz_cut = abs(taus.dz) < 0.2
        return taus[pt_cut & eta_cut & dz_cut & id_cut]

    def select_jets(self, jets: awkward.highlevel.Array, jet_pt_threshold: float, jet_max_eta: float) -> awkward.highlevel.Array:
        """
        Selects jets from the input `jets` array based on specified criteria.

        Args:
            jets (awkward.highlevel.Array): The input array containing jet objects.
            jet_pt_threshold (float): The minimum transverse momentum threshold for jet selection.
            jet_max_eta (float): The maximum absolute rapidity for jet selection.

        Returns:
            awkward.highlevel.Array: The selected jets satisfying the given criteria.

        Example:
            jets = ...  # Initialize your awkward.highlevel.Array of jets
            selected_jets = select_ejst(jets, jet_pt_threshold=30.0, jet_max_eta=4.7)

        Note:
            - The provided `jets` should be an `awkward.highlevel.Array` containing jet objects.
            - The function applies selection criteria on the `jets` array based on the following conditions:
                - Transverse momentum (pt) greater than `jet_pt_threshold`.
                - Pseudorapidity (eta) within the range (-`jet_max_eta`, +`jet_max_eta`).
            - The function returns the jets that satisfy all the specified selection criteria.

        """
        pt_cut = jets.pt > jet_pt_threshold
        eta_cut = abs(jets.eta) < abs(jet_max_eta)
        # apply the tight jet ID
        id_cut = (jets.jetId & 3) != 0
        return jets[pt_cut & eta_cut & id_cut]

    def add_pair_quantities(self, pairs: awkward.Array) -> awkward.Array:
        """
        Adds quantities to pairs of objects and returns the modified pairs.

        Args:
            pairs (awkward.Array): An Awkward Array containing pairs of objects.

        Returns:
            awkward.Array: The modified pairs with additional quantities.

        - The pairs are transformed into candidates with four momenta and additional properties.

        The modified pairs are returned as an Awkward Array with the name 'PtEtaPhiMCandidate'.
        """

        logger.debug("Adding ditau pair quantities")
        # turn the pairs into candidates with four momenta and such
        pair_4mom = pairs["obj_1"] + pairs["obj_2"]
        pairs["pt"] = pair_4mom.pt
        pairs["eta"] = pair_4mom.eta
        pairs["phi"] = pair_4mom.phi
        pairs["mass"] = pair_4mom.mass
        pairs["charge"] = pair_4mom.charge
        pairs["os"] = pair_4mom.charge == 0
        pairs["dR"] = pairs["obj_1"].delta_r(pairs["obj_2"])
        pairs = awkward.with_name(pairs, "PtEtaPhiMCandidate")
        pairs['dphi'] = calculate_delta_phi(pairs["obj_1"].phi,pairs["obj_2"].phi)

        return pairs

    def add_lhe_weights(self, pairs: awkward.Array, events: awkward.Array, dataset_name: str) -> awkward.Array:
        """
        Adds LHE weights to pairs of objects and returns the modified pairs.

        Args:
            pairs (awkward.Array): An Awkward Array containing pairs of objects.
            events (awkward.Array): An Awkward Array containing the events data.

        Returns:
            awkward.Array: The modified pairs with additional LHE weights.

        - The LHE weights are added to the pairs of objects.
        """

        logger.debug("Adding LHE weights")
        if "ProdAndDecay" in dataset_name:
            pairs["LHEReweightingWeight_SM"] = events.LHEReweightingWeight[:,0]
            pairs["LHEReweightingWeight_PS"] = events.LHEReweightingWeight[:,1]
            pairs["LHEReweightingWeight_MM"] = events.LHEReweightingWeight[:,2]
        else:
            pairs["LHEReweightingWeight_SM"] = awkward.ones_like(pairs["obj_1"].pt)
            pairs["LHEReweightingWeight_PS"] = awkward.ones_like(pairs["obj_1"].pt)
            pairs["LHEReweightingWeight_MM"] = awkward.ones_like(pairs["obj_1"].pt)

        return pairs

    def add_cp_quantities(self, pairs: awkward.Array, events: awkward.Array, channel: str) -> awkward.Array:

        logger.debug("Adding CP quantities")

        wt_cp_sm = awkward.zeros_like(pairs["obj_1"].pt)
        wt_cp_ps = awkward.zeros_like(pairs["obj_1"].pt)
        wt_cp_mm = awkward.zeros_like(pairs["obj_1"].pt)

        # check if "TauSpinner is present in the events"
        if "TauSpinner" in events.fields:
            wt_cp_sm = events.TauSpinner.weight_cp_0
            wt_cp_ps = events.TauSpinner.weight_cp_0p5
            wt_cp_mm = events.TauSpinner.weight_cp_0p25

        pairs["wt_cp_sm"] = wt_cp_sm
        pairs["wt_cp_ps"] = wt_cp_ps
        pairs["wt_cp_mm"] = wt_cp_mm

        pairs["pv_x"] = awkward.flatten(events.PVBS.x)
        pairs["pv_y"] = awkward.flatten(events.PVBS.y)
        pairs["pv_z"] = awkward.flatten(events.PVBS.z)

        pairs["ip_x_1"] = pairs["obj_1"].IPx
        pairs["ip_y_1"] = pairs["obj_1"].IPy
        pairs["ip_z_1"] = pairs["obj_1"].IPz
        pairs["ip_x_2"] = pairs["obj_2"].IPx
        pairs["ip_y_2"] = pairs["obj_2"].IPy
        pairs["ip_z_2"] = pairs["obj_2"].IPz

        for index in ['1', '2']:
            if 'hasRefitSV' in pairs[f'obj_{index}'].fields:
                pairs[f"hasRefitSV_{index}"] = pairs[f"obj_{index}"].hasRefitSV
                pairs[f"sv_x_{index}"] = pairs[f"obj_{index}"].refitSVx
                pairs[f"sv_y_{index}"] = pairs[f"obj_{index}"].refitSVy
                pairs[f"sv_z_{index}"] = pairs[f"obj_{index}"].refitSVz
            else:
                pairs[f"hasRefitSV_{index}"] = awkward.zeros_like(pairs[f"obj_{index}"].pt)
                pairs[f"sv_x_{index}"] = awkward.ones_like(pairs[f"obj_{index}"].IPx) * -9999.0
                pairs[f"sv_y_{index}"] = awkward.ones_like(pairs[f"obj_{index}"].IPy) * -9999.0
                pairs[f"sv_z_{index}"] = awkward.ones_like(pairs[f"obj_{index}"].IPz) * -9999.0

        pdgId_to_mass = {
            211: 0.13957,
            22: 0.0,
            -211: 0.13957,
        }

        neutral_pion_mass = 0.13498

        particle_names = ['pi', 'pi2', 'pi3', 'pi0']
        components = ['pt', 'eta', 'phi', 'mass', 'charge']

        def assign_values(pairs, particle, index, value_dict):
            for component in components:
                pairs[f"{particle}_{component}_{index}"] = value_dict[component]

        for index,lepton in enumerate(channel):
            index += 1
            if lepton == 't':
                tau_index = pairs[f"obj_{index}"].original_index
                tauProds = events.TauProd
                mask = tauProds.tauIdx == awkward.broadcast_arrays(tau_index, tauProds.tauIdx)[0]
                tauProds = tauProds[mask]

                mass = -9999.0 * awkward.ones_like(awkward.flatten(tauProds.pdgId))
                for pdgId, mass_value in pdgId_to_mass.items():
                    mass = awkward.where(awkward.flatten(tauProds.pdgId) == pdgId, mass_value, mass)
                mass = awkward.unflatten(mass, awkward.num(tauProds))
                tauProds = awkward.with_field(tauProds, mass, "mass")

                pion_mask = (tauProds.pdgId == 211) | (tauProds.pdgId == -211)
                photon_mask = (tauProds.pdgId) == 22

                pions = tauProds[pion_mask]
                pions = awkward.fill_none(awkward.pad_none(pions, 3, clip=True), {"pt": -9999.0, "eta": -9999.0, "phi": -9999.0, "pdgId": -9999.0, "mass": -9999.0})
                charges = awkward.where(pions.pdgId == 211, 1, -1)
                pions = awkward.with_field(pions, charges, "charge")
                pions_p4 = calculate_p4(pions)

                photons = tauProds[photon_mask]
                photons = photons[awkward.argsort(photons.pt, ascending=False)]
                photons_p4 = calculate_p4(photons)

                pi0_eta = awkward.fill_none(awkward.firsts(photons.eta, axis=-1), -9999.0)
                pi0_phi = awkward.fill_none(awkward.firsts(photons.phi, axis=-1), -9999.0)
                pi0_mass = neutral_pion_mass * awkward.ones_like(pi0_eta)
                sum_photons_p4 = sum_p4(photons_p4, axis=-1)
                pi0_pt = awkward.where(sum_photons_p4.pt == 0, -9999.0, sum_photons_p4.pt)
                pi0_pdgId = 111 * awkward.ones_like(pi0_eta)

                pi0 = awkward.zip(
                    {
                        "pt": pi0_pt,
                        "eta": pi0_eta,
                        "phi": pi0_phi,
                        "mass": pi0_mass,
                        "pdgId": pi0_pdgId,
                        "charge": awkward.zeros_like(pi0_pt),
                    }
                )
                pi0_p4 = calculate_p4(pi0)

                for i, particle in enumerate(particle_names[:3]):
                    value_dict = {comp: getattr(pions_p4[:, i], comp) for comp in components}
                    assign_values(pairs, particle, index, value_dict)

                value_dict = {comp: getattr(pi0_p4, comp) for comp in components}
                assign_values(pairs, particle_names[3], index, value_dict)

            else:
                default_value = -9999.0 * awkward.ones_like(pairs['obj_1'].pt)
                for particle in particle_names:
                    value_dict = {comp: default_value for comp in components}
                    assign_values(pairs, particle, index, value_dict)

        return pairs

    def add_met_quantities(self, pairs: awkward.Array, met: awkward.Array) -> awkward.Array:
        """
        Adds MET-related quantities to pairs of objects and returns the modified pairs.

        Args:
            pairs (awkward.Array): An Awkward Array containing pairs of objects.
            met (awkward.Array): An Awkward Array containing the MET.

        Returns:
            awkward.Array: The modified pairs with additional MET-related quantities.
        """

        logger.debug("Adding MET quantities")

        # met related quantities
        pairs["met_pt"] = met.pt
        pairs["met_phi"] = met.phi
        pairs["met_covXX"] = met.covXX
        pairs["met_covYY"] = met.covYY
        pairs["met_covXY"] = met.covXY
        pairs["met_dphi_1"] = calculate_delta_phi(pairs["obj_1"].phi,met.phi)
        pairs["met_dphi_2"] = calculate_delta_phi(pairs["obj_2"].phi,met.phi)
        pairs['mt_1'] = calculate_transverse_mass(pairs["obj_1"],met)
        pairs['mt_2'] = calculate_transverse_mass(pairs["obj_2"],met)
        pairs['mt_lep'] = calculate_transverse_mass(pairs["obj_1"],pairs["obj_2"])
        pairs['mt_tot'] = numpy.sqrt(pairs.mt_1**2 + pairs.mt_2**2 + pairs.mt_lep**2)
        pairs['pt_tt'] = calculate_transverse_momentum(pairs["obj_1"],pairs["obj_2"])
        pairs['pt_tt_met'] = calculate_transverse_momentum(pairs,met)

        return pairs

    def add_jet_quantities(self, pairs: awkward.Array, jets: awkward.Array, seeding_string: str) -> awkward.Array:
        """
        Adds jet-related quantities to pairs of objects and returns the modified pairs.

        Args:
            pairs (awkward.Array): An Awkward Array containing pairs of objects.
            jets (awkward.Array): An Awkward Array containing the jets.
            seeding_string (str): The string to use for seeding jets.

        Returns:
            awkward.Array: The modified pairs with additional jet-related quantities.
        """

        logger.debug("Adding jet quantities")

        # ensure jets are ordered by pT
        jets = jets[awkward.argsort(jets.pt, ascending=False)]

        # set charge of all jets to 0 as it is not defined
        jets["charge"] = awkward.zeros_like(jets.pt)

        default_value = -9999.

        lead_jet = awkward.zip(
            {
                "pt": choose_jet(jets.pt, 0, default_value),
                "eta": choose_jet(jets.eta, 0, default_value),
                "phi": choose_jet(jets.phi, 0, default_value),
                "mass": choose_jet(jets.mass, 0, default_value),
                "charge":choose_jet(jets.charge, 0, default_value),
            }
        )
        lead_jet = awkward.with_name(lead_jet, "PtEtaPhiMCandidate")

        sublead_jet = awkward.zip(
            {
                "pt": choose_jet(jets.pt, 1, default_value),
                "eta": choose_jet(jets.eta, 1, default_value),
                "phi": choose_jet(jets.phi, 1, default_value),
                "mass": choose_jet(jets.mass, 1, default_value),
                "charge":choose_jet(jets.charge, 1, default_value),
            }
        )
        sublead_jet = awkward.with_name(sublead_jet, "PtEtaPhiMCandidate")

        pairs[f'{seeding_string}jet_1'] = lead_jet
        pairs[f'{seeding_string}jet_2'] = sublead_jet

        lead_jet_masked = awkward.zip(
            {
                "pt": choose_jet(jets.pt, 0, 0),
                "eta": choose_jet(jets.eta, 0, 0),
                "phi": choose_jet(jets.phi, 0, 0),
                "mass": choose_jet(jets.mass, 0, 0),
                "charge":choose_jet(jets.charge, 0, 0),
            }
        )
        lead_jet_masked = awkward.with_name(lead_jet_masked, "PtEtaPhiMCandidate")

        sublead_jet_masked = awkward.zip(
            {
                "pt": choose_jet(jets.pt, 1, 0),
                "eta": choose_jet(jets.eta, 1, 0),
                "phi": choose_jet(jets.phi, 1, 0),
                "mass": choose_jet(jets.mass, 1, 0),
                "charge":choose_jet(jets.charge, 1, 0),
            }
        )
        sublead_jet_masked = awkward.with_name(sublead_jet_masked, "PtEtaPhiMCandidate")

        dijet = lead_jet_masked + sublead_jet_masked
        dijet = awkward.with_name(dijet, "Candidate")

        pairs[f"{seeding_string}jpt_1"] = lead_jet.pt
        pairs[f"{seeding_string}jeta_1"] = lead_jet.eta
        pairs[f"{seeding_string}jphi_1"] = lead_jet.phi
        pairs[f"{seeding_string}jpt_2"] = sublead_jet.pt
        pairs[f"{seeding_string}jeta_2"] = sublead_jet.eta
        pairs[f"{seeding_string}jphi_2"] = sublead_jet.phi
        pairs[f"{seeding_string}n_jets"] = awkward.num(jets)

        pairs[f"{seeding_string}dijetpt"] = awkward.where(
            pairs[f"{seeding_string}n_jets"] >= 2,
            awkward.where(
                (lead_jet.pt != default_value) & (sublead_jet.pt != default_value),
                dijet.pt,
                default_value
            ),
            default_value
        )

        pairs[f"{seeding_string}mjj"] = awkward.where(
            pairs[f"{seeding_string}n_jets"] >= 2,
            awkward.where(
                (lead_jet.pt != default_value) & (sublead_jet.pt != default_value),
                dijet.mass,
                default_value
            ),
            default_value
        )

        pairs[f"{seeding_string}jdeta"] = awkward.where(pairs[f"{seeding_string}n_jets"] >= 2, numpy.abs(pairs[f"{seeding_string}jeta_1"] - pairs[f"{seeding_string}jeta_2"]), default_value)
        # signed jet delta phi, where sign is defined by eta ordering of the jets
        sjdphi = awkward.where(lead_jet.eta >= sublead_jet.eta, calculate_delta_phi(lead_jet.phi,sublead_jet.phi), calculate_delta_phi(sublead_jet.phi,lead_jet.phi))
        pairs[f"{seeding_string}sjdphi"] = awkward.where(pairs[f"{seeding_string}n_jets"] >= 2, sjdphi, default_value)

        return pairs

    def add_pair_gen_info(self, events: awkward.Array, pairs: awkward.Array, selections: str) -> awkward.Array:
        logger.debug("Adding gen info to pairs")

        default_value = -9999.

        selected_events = events[selections]

        pairs["genPartFlav_1"] = pairs["obj_1"].genPartFlav
        pairs["genPartFlav_2"] = pairs["obj_2"].genPartFlav
        genpart_idx_1 = pairs["obj_1"].genPartIdx
        genpart_idx_2 = pairs["obj_2"].genPartIdx

        dummy_record_genPart = awkward.Record({'eta': default_value, 'mass': default_value, 'phi': default_value, 'pt': default_value, 'genPartIdxMother': default_value, 'pdgId': default_value, 'status': default_value, 'statusFlags': default_value, 'genPartIdxMotherG': default_value, 'distinctParentIdxG': default_value, 'childrenIdxG': [], 'distinctChildrenIdxG': [], 'distinctChildrenDeepIdxG': []})
        genPart = selected_events.GenPart
        genpart_1 = awkward.Array([sublist[index] if index < len(sublist) and index != -1 and len(sublist) != 0 else dummy_record_genPart for sublist, index in zip(genPart, genpart_idx_1)])

        genpart_2 = awkward.Array([sublist[index] if index < len(sublist) and index != -1 and len(sublist) != 0 else dummy_record_genPart for sublist, index in zip(genPart, genpart_idx_2)])

        pairs["genPart_eta_1"] = genpart_1.eta
        pairs["genPart_eta_2"] = genpart_2.eta
        pairs["genPart_mass_1"] = genpart_1.mass
        pairs["genPart_mass_2"] = genpart_2.mass
        pairs["genPart_pdgId_1"] = genpart_1.pdgId
        pairs["genPart_pdgId_2"] = genpart_2.pdgId
        pairs["genPart_phi_1"] = genpart_1.phi
        pairs["genPart_phi_2"] = genpart_2.phi
        pairs["genPart_pt_1"] = genpart_1.pt
        pairs["genPart_pt_2"] = genpart_2.pt
        pairs["genPart_statusFlags_1"] = genpart_1.statusFlags
        pairs["genPart_statusFlags_2"] = genpart_2.statusFlags

        dummy_record_genVisTau = awkward.Record({'eta': default_value, 'mass': default_value, 'phi': default_value, 'pt': default_value, 'charge': default_value, 'genPartIdxMother': default_value, 'status': default_value, 'genPartIdxMotherG': default_value})
        genVisTau = selected_events.GenVisTau
        genvistau_1 = awkward.Array([sublist[index] if index < len(sublist) and index != -1 and len(sublist) != 0 else dummy_record_genVisTau for sublist, index in zip(genVisTau, genpart_idx_1)])
        genvistau_2 = awkward.Array([sublist[index] if index < len(sublist) and index != -1 and len(sublist) != 0 else dummy_record_genVisTau for sublist, index in zip(genVisTau, genpart_idx_2)])

        pairs['genvistau_charge_1'] = genvistau_1.charge
        pairs['genvistau_charge_2'] = genvistau_2.charge
        pairs['genvistau_eta_1'] = genvistau_1.eta
        pairs['genvistau_eta_2'] = genvistau_2.eta
        pairs['genvistau_genPartIdxMother_1'] = genvistau_1.genPartIdxMother
        pairs['genvistau_genPartIdxMother_2'] = genvistau_2.genPartIdxMother
        pairs['genvistau_mass_1'] = genvistau_1.mass
        pairs['genvistau_mass_2'] = genvistau_2.mass
        pairs['genvistau_phi_1'] = genvistau_1.phi
        pairs['genvistau_phi_2'] = genvistau_2.phi
        pairs['genvistau_pt_1'] = genvistau_1.pt
        pairs['genvistau_pt_2'] = genvistau_2.pt
        pairs['genvistau_status_1'] = genvistau_1.status
        pairs['genvistau_status_2'] = genvistau_2.status

        return pairs

    def apply_met_filters(self, events: awkward.Array) -> awkward.Array:
        """
        Apply MET filters to the events and return the filtered events.

        Args:
            events (awkward.Array): An Awkward Array containing the events data.

        Returns:
            awkward.Array: The filtered events after applying MET filters.

        This function applies MET filters to the events based on a list of filters specified in the metaconditions.
        Note: The MET filters are applied using the logical AND operation on the individual filter flags.
        """

        logger.debug("Applying MET filters")
        # met filters
        met_filters = self.meta["MetFilters"][self.data_kind]
        filtered = functools.reduce(
            operator.and_,
            (events.Flag[metfilter.split("_")[-1]] for metfilter in met_filters),
        )
        return events[filtered]

    def get_trigger_OR_mask(self, objects : awkward.Array, trig_objects : awkward.Array, selections : str, dR : float) -> awkward.Array:
        """
        Get the trigger OR mask for objects based on a list of trigger selections and delta R matching.

        This function applies a set of trigger selections on the 'trig_objects' and returns a mask that
        represents the logical OR (disjunction) of those selections. It also performs delta R matching
        between the selected trigger objects and 'objects', returning the matched objects.

        Parameters:
            objects (awkward.Array): The array of objects for delta R matching.
            trig_objects (awkward.Array): The array of trigger objects for applying selections.
            selections (str): A string containing trigger selections separated by ':' for logical OR.
                              Individual selections are separated by ','. Supported comparisons are '==', '=', '>=',
                              '>', '<=', and '<'. Bitwise AND operation is supported with '&'.
                              Example: 'id==13,pt>30,filterBits&3'
            dR (float): The delta R threshold for matching objects.

        Returns:
            awkward.Array: An array with boolean values indicating if the objects passed the delta R matching and
                           trigger OR selection. The array contains 'True' for objects that have matching trigger
                           objects according to the specified selections and delta R threshold, and 'False' otherwise.

        Note:
            If no trigger selections are specified (or '1' or 'None' is provided), the function returns an array
            that is 'True' for non-None values (i.e., there is an offline object) and 'False' for None values.
        """

        # if no trigger selections were specified then the object is taken to have passed the selection
        allowed_operations = set(['==', '=', '>=', '>', '<=', '<', '&'])
        selections_check = selections.strip().lower()  # Convert to lowercase and remove leading/trailing spaces
        if selections_check == '1' or selections_check == 'none' or not any(op in selections for op in allowed_operations):
            # return an array that is false for none value and true for non-None values(i.e there is an offline object)
            return ~awkward.is_none(objects)

        masks_for_OR = []
        for sels in selections.split(':'):
            sels_vec = sels.split(',')
            AND_mask = trig_objects.filterBits > 0
            for sel in sels_vec:
                sel_no_spaces = sel.replace(' ','')
                if '==' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('==')[0]] == float(sel_no_spaces.split('==')[1]))
                elif '=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('=')[0]] == float(sel_no_spaces.split('=')[1]))
                elif '>=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('>=')[0]] >= float(sel_no_spaces.split('>=')[1]))
                elif '>' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('>')[0]] > float(sel_no_spaces.split('>')[1]))
                elif '<=' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('<=')[0]] <= float(sel_no_spaces.split('<=')[1]))
                elif '<' in sel_no_spaces:
                    AND_mask = AND_mask & (trig_objects[sel_no_spaces.split('<')[0]] > float(sel_no_spaces.split('<')[1]))
                elif '&' in sel_no_spaces:
                    AND_mask = AND_mask & ((trig_objects[sel_no_spaces.split('&')[0]] & int(sel_no_spaces.split('&')[1])) != 0)
            masks_for_OR.append(AND_mask)

        # Combine all masks with logical OR operation
        mask = awkward.any(masks_for_OR,axis=0)

        # passing trigger objects are selected by requiring them to pass the OR selection AND their is a corresponding offline object
        passing_trig_objects = trig_objects[mask & ~awkward.is_none(objects)]

        # Apply delta R matching and get the matched objects
        matched_objects = passing_trig_objects[delta_r_match_mask(passing_trig_objects, objects, dR)]

        # return an array that is false if there are no matched trigger objects or true if there are matches
        return awkward.num(matched_objects) > 0

    def apply_triggers(self, trig_objects: awkward.Array, pairs: awkward.Array, outname : str, leg1_selections: str = None, leg2_selections : str = None, leg3_selections : str = None, leg4_selections : str = None) -> awkward.Array:
        """
        Apply triggers to candidate pairs in the provided 'pairs' array based on trigger matching criteria.

        Parameters:
            trig_objects (awkward.Array): Awkward Array containing the trigger objects.
            pairs (awkward.Array): Awkward Array containing the candidate pairs.
            outname (str): Name of the output column that will be added to the 'pairs' array to indicate trigger matches.
            leg1_selections (str): Trigger matching criteria for the first leg (obj_1) of candidate pairs.
            leg2_selections (str): Trigger matching criteria for the second leg (obj_2) of candidate pairs.

        Returns:
            awkward.Array: Awkward Array containing the candidate pairs with an additional bool column indicating trigger matches.
        """

        # get trigger matched for first leg (obj_1)
        if leg1_selections:
            leg1_matches = self.get_trigger_OR_mask(pairs['obj_1'],trig_objects,leg1_selections,0.5)
        else:
            leg1_matches = True
        # get trigger matched for second leg (obj_2)
        if leg2_selections:
            leg2_matches = self.get_trigger_OR_mask(pairs['obj_2'],trig_objects,leg2_selections,0.5)
        else:
            leg2_matches = True
        # get trigger matched for third leg (leading jet)
        if leg3_selections:
            leg3_matches = self.get_trigger_OR_mask(pairs['jet_1'],trig_objects,leg3_selections,0.5)
            # default phi and eta of jets that don't exist should be -9999 so dR matching for trigger objects should
            # never pass anyway. But to be extra careful we set the matches to False if the leading jet does not exist (n_jets<1)
            leg3_matches = awkward.where(pairs["n_jets"] >= 1, leg3_matches, False)
        else:
            leg3_matches = True
        # get trigger matched for fourth leg (subleading jet)
        if leg4_selections:
            leg4_matches = self.get_trigger_OR_mask(pairs['jet_2'],trig_objects,leg4_selections,0.5)
            # default phi and eta of jets that don't exist should be -9999 so dR matching for trigger objects should
            # never pass anyway. But to be extra careful we set the matches to False if the subleading jet does not exist (n_jets<2)
            leg4_matches = awkward.where(pairs["n_jets"] >= 2, leg4_matches, False)
        else:
            leg4_matches = True

        # now add a bool as a variable to indicate if the trigger passed or not (i.e both legs pass)
        pairs[outname] = leg1_matches & leg2_matches & leg3_matches & leg4_matches

        return pairs

    def rename_and_drop(self, array: awkward.Array, channel: str, data_kind: str, correction_names: list) -> awkward.Array:
        """
        Renames and drops columns in the provided `awkward.Array` based on a predefined mapping.

        Args:
            array (awkward.Array): The input array containing columns to be renamed and dropped.
            channel (str): A string that I defines the ditau channel ('mm','ee','et','mt','em','tt') which determines which quantities are stored

        Returns:
            awkward.Array: The modified array with columns renamed and dropped based on the mapping.

        Example:
            array = ...  # Initialize your awkward.Array
            modified_array = rename_and_drop(array,'tt')

        Note:
            - The provided `array` should be an `awkward.Array`.
            - The function uses the `awkward.zip` function to rename and drop columns based on the mapping.
            - Columns with `None` values in the mapping will retain their original names.
            - Columns not present in the mapping will be dropped from the resulting array.
        """

        # TODO: map is predefined for now, but could be passed as an argument instead
        map_dict = {
            "charge_1": None,
            "charge_2": None,
            "event" : None,
            "run" : None,
            "lumi" : None,
            "pt_1" : None,
            "pt_2" : None,
            "eta_1" : None,
            "eta_2" : None,
            "phi_1": None,
            "phi_2": None,
            "mass_1": None,
            "mass_2": None,
            "dR" : None,
            "q_1" : None,
            "q_2" : None,
            "pt_tt" : None,
            "pt_tt_met" : None,
            "mt_1" : None,
            "mt_2" : None,
            "mt_lep" : None,
            "mt_tot" : None,
            "os" : None,
            "met_pt" : None,
            "met_phi" : None,
            "met_covXX" : None,
            "met_covXY" : None,
            "met_covYY" : None,
            "met_dphi_1" : None,
            "met_dphi_2" : None,
            "dphi" : None,
            "trg_singlemuon" : None,
            "trg_singleelectron" : None,
            "trg_singleelectron_2" : None,
            "trg_singlemuon_2" : None,
            "trg_doubletau" : None,
            "trg_doubletauandjet": "",
            "trg_doubletauandjet_2": "",
            "idDeepTau2018v2p5VSjet_1" : "",
            "idDeepTau2018v2p5VSjet_2" : "",
            "idDeepTau2018v2p5VSmu_1" : "",
            "idDeepTau2018v2p5VSmu_2" : "",
            "idDeepTau2018v2p5VSe_1" : "",
            "idDeepTau2018v2p5VSe_2" : "",
            "mass" : "m_vis",
            "pt" : "pt_vis",
            "n_jets": "",
            "n_prebjets": "",
            "n_bjets": "",
            "mjj": "",
            "jdeta": "",
            "sjdphi": "",
            "dijetpt": "",
            "jpt_1": "",
            "jpt_2": "",
            "jeta_1": "",
            "jeta_2": "",
            "jphi_1": "",
            "jphi_2": "",
            "weight": "",
            "decayMode_1": "",
            "decayMode_2": "",
            "rawDeepTau2018v2p5VSe_1" : "",
            "rawDeepTau2018v2p5VSe_2" : "",
            "rawDeepTau2018v2p5VSjet_1" : "",
            "rawDeepTau2018v2p5VSjet_2" : "",
            "rawDeepTau2018v2p5VSmu_1" : "",
            "rawDeepTau2018v2p5VSmu_2" : "",
            "seeding_n_jets": "",
            "seeding_mjj": "",
            "seeding_jdeta": "",
            "seeding_sjdphi": "",
            "seeding_dijetpt": "",
            "seeding_jpt_1": "",
            "seeding_jpt_2": "",
            "seeding_jeta_1": "",
            "seeding_jeta_2": "",
            "seeding_jphi_1": "",
            "seeding_jphi_2": "",
            "LHEReweightingWeight_SM": "",
            "LHEReweightingWeight_PS": "",
            "LHEReweightingWeight_MM": "",
            "wt_cp_sm": "",
            "wt_cp_ps": "",
            "wt_cp_mm": "",
            "pv_x": "",
            "pv_y": "",
            "pv_z": "",
            "hasRefitSV_1": "",
            "hasRefitSV_2": "",
            "ip_x_1": "",
            "ip_y_1": "",
            "ip_z_1": "",
            "ip_x_2": "",
            "ip_y_2": "",
            "ip_z_2": "",
            "sv_x_1": "",
            "sv_y_1": "",
            "sv_z_1": "",
            "sv_x_2": "",
            "sv_y_2": "",
            "sv_z_2": "",
            "pi_pt_1": "",
            "pi_eta_1": "",
            "pi_phi_1": "",
            "pi_mass_1": "",
            "pi_charge_1": "",
            "pi2_pt_1": "",
            "pi2_eta_1": "",
            "pi2_phi_1": "",
            "pi2_mass_1": "",
            "pi2_charge_1": "",
            "pi3_pt_1": "",
            "pi3_eta_1": "",
            "pi3_phi_1": "",
            "pi3_mass_1": "",
            "pi3_charge_1": "",
            "pi0_pt_1": "",
            "pi0_eta_1": "",
            "pi0_phi_1": "",
            "pi0_mass_1": "",
            "pi0_charge_1": "",
            "pi_pt_2": "",
            "pi_eta_2": "",
            "pi_phi_2": "",
            "pi_mass_2": "",
            "pi_charge_2": "",
            "pi2_pt_2": "",
            "pi2_eta_2": "",
            "pi2_phi_2": "",
            "pi2_mass_2": "",
            "pi2_charge_2": "",
            "pi3_pt_2": "",
            "pi3_eta_2": "",
            "pi3_phi_2": "",
            "pi3_mass_2": "",
            "pi3_charge_2": "",
            "pi0_pt_2": "",
            "pi0_eta_2": "",
            "pi0_phi_2": "",
            "pi0_mass_2": "",
            "pi0_charge_2": "",
        }

        if channel == "ee":
            map_dict["miniPFRelIso_all_1"] = "iso_1"
            map_dict["miniPFRelIso_all_2"] = "iso_2"
        if channel == "mm":
            map_dict["pfRelIso04_all_1"] = "iso_1"
            map_dict["pfRelIso04_all_2"] = "iso_2"
        if channel == "em":
            map_dict["pfRelIso04_all_1"] = "iso_1"
            map_dict["miniPFRelIso_all_2"] = "iso_2"
        if channel in ["et"]:
            map_dict["miniPFRelIso_all_1"] = "iso_1"
        if channel in ["mt"]:
            map_dict["pfRelIso04_all_1"] = "iso_1"

        if data_kind == "mc":  # add MC specific info
            map_dict["genPartFlav_1"] = ""
            map_dict["genPartFlav_2"] = ""
            map_dict["genPart_eta_1"] = ""
            map_dict["genPart_eta_2"] = ""
            map_dict["genPart_mass_1"] = ""
            map_dict["genPart_mass_2"] = ""
            map_dict["genPart_pdgId_1"] = ""
            map_dict["genPart_pdgId_2"] = ""
            map_dict["genPart_phi_1"] = ""
            map_dict["genPart_phi_2"] = ""
            map_dict["genPart_pt_1"] = ""
            map_dict["genPart_pt_2"] = ""
            map_dict["genPart_statusFlags_1"] = ""
            map_dict["genPart_statusFlags_2"] = ""

            map_dict['genvistau_charge_1'] = ""
            map_dict['genvistau_charge_2'] = ""
            map_dict['genvistau_eta_1'] = ""
            map_dict['genvistau_eta_2'] = ""
            map_dict['genvistau_genPartIdxMother_1'] = ""
            map_dict['genvistau_genPartIdxMother_2'] = ""
            map_dict['genvistau_mass_1'] = ""
            map_dict['genvistau_mass_2'] = ""
            map_dict['genvistau_phi_1'] = ""
            map_dict['genvistau_phi_2'] = ""
            map_dict['genvistau_pt_1'] = ""
            map_dict['genvistau_pt_2'] = ""
            map_dict['genvistau_status_1'] = ""
            map_dict['genvistau_status_2'] = ""

            map_dict['w_DY_soup'] = ""
            map_dict['w_WJ_soup'] = ""

            for correction_name in correction_names:
                map_dict["w_" + correction_name] = ""

        # Drop columns not in the map and rename columns based on the map
        array_renamed = awkward.zip(
            {new_name if not (new_name == '' or new_name is None) else old_name: array[old_name]
             for old_name, new_name in map_dict.items() if old_name in array.fields}
        )

        return array_renamed

    def process(self, events: awkward.Array) -> Dict[Any, Any]:

        # here we start recording possible coffea accumulators
        # most likely histograms, could be counters, arrays, ...
        histos_etc = {}

        dataset_name = events.metadata["dataset"]

        # data or monte carlo?
        self.data_kind = "mc" if hasattr(events, "GenPart") else "data"

        # Store effective number of events (before selection)
        if self.data_kind == "mc":
            pos = awkward.count_nonzero(events.genWeight > 0)
            neg = awkward.count_nonzero(events.genWeight < 0)
            n_eff = pos - neg
        else:
            n_eff = len(events)

        if self.run_effective:
            if self.data_kind == "mc":
                # Define the output path and file name
                out_path = f"{self.output_location}/{self.year}/{dataset_name}/"
                if not os.path.exists(out_path):
                    os.makedirs(out_path)

                fname = (
                    events.behavior["__events_factory__"]._partition_key.replace(
                        "/", "_"
                    )
                    + ".parquet"
                )

                run_info_file = f"{out_path}/{fname.split('.')[0]}.txt"
                with open(run_info_file, 'w') as txtout:
                    txtout.write(str(n_eff))

            return histos_etc

        # metadata array to append to higgsdna output
        metadata = {}

        # lumi mask
        if self.data_kind == "data":
            try:
                lumimask = select_lumis(self.year, events, logger)
                events = events[lumimask]
                logger.info(f"[ lumimask ] Applied lumi mask for {self.year} on {dataset_name}")
            except:
                logger.info(
                    f"[ lumimask ] Skip now! Unable to find year info of {dataset_name}"
                )

        # apply jetvetomap: only retain events that without any jets in the EE leakage region
        events = jetvetomap(
            events, logger, dataset_name, year=self.year
        )

        if 'Channels' not in self.meta:
            channels = "all"
        else:
            channels = self.meta['Channels']
        if channels == "all":
            channels = ['mm','ee','em','et','mt','tt']
        else:
            channels = channels.split(',')
        logger.debug(f"Processing channels: {channels}")

        # read which systematics and corrections to process
        try:
            correction_names = self.corrections
        except KeyError:
            correction_names = []
        try:
            systematics = self.systematics
        except KeyError:
            systematics = []

        # apply met filters
        events = self.apply_met_filters(events)

        electrons_dct = {}
        muons_dct = {}
        taus_dct = {}
        jets_dct = {}
        met_dct = {}

        systematic_variations = []
        systematic_variations.append("nominal")

        if self.data_kind == "mc":
            for correction_name in correction_names:
                if correction_name in available_object_corrections.keys():
                    logger.info(
                        f"Applying correction {correction_name} to dataset {dataset_name}"
                    )
                    varying_function = available_object_corrections[correction_name]
                    events = varying_function(events=events,year=self.year)

        if self.data_kind == "mc":
            for systematic_name in systematics:
                if systematic_name in available_object_systematics.keys():
                    systematic_dct = available_object_systematics[systematic_name]
                    logger.info(
                        f"Applying systematic {systematic_name} to tau collections"
                    )
                    if systematic_dct["args"]["kind"] == "UpDownSystematic":
                        systematic_variations.append(systematic_name + "_up")
                        systematic_variations.append(systematic_name + "_down")
                    if not systematic_dct["args"]["coffea"]:
                        varying_function = systematic_dct["args"]["varying_function"]
                        events_up,events_down = varying_function(events=events,year=self.year)
                        if systematic_dct["object"] == "Tau":
                            taus_dct[systematic_name + "_up"] = events_up
                            taus_dct[systematic_name + "_down"] = events_down

        electrons_dct["nominal"] = events.Electron
        muons_dct["nominal"] = events.Muon
        taus_dct["nominal"] = events.Tau
        jets_dct["nominal"] = events.Jet
        met_dct["nominal"] = events[self.meta['MET_Type']]

        for variation in systematic_variations:
            logger.debug(f"Variation: {variation}")
            electrons,muons,taus = electrons_dct["nominal"],muons_dct["nominal"],taus_dct["nominal"]
            met_, jets_ = met_dct["nominal"], jets_dct["nominal"]
            if variation == "nominal":
                pass
            elif variation in [*electrons_dct]:
                electrons = electrons_dct[variation]
            elif variation in [*muons_dct]:
                muons = muons_dct[variation]
            elif variation in [*taus_dct]:
                taus = taus_dct[variation]
            elif variation in [*jets_dct]:
                jets_ = jets_dct[variation]
            elif variation in [*met_dct]:
                met_ = met_dct[variation]

            electrons = awkward.with_field(electrons, awkward.local_index(electrons), 'original_index')
            muons = awkward.with_field(muons, awkward.local_index(muons), 'original_index')
            taus = awkward.with_field(taus, awkward.local_index(taus), 'original_index')

            # object preselection
            # first we select muons using loose pt and isolation cuts so that they can be used in all channels and eventually as veto objects
            logger.debug("Applying muon preselection")
            muons = self.select_muons(muons, 10., 2.4)

            # then we do the same for electrons
            logger.debug("Applying electron preselection")
            electrons = self.select_electrons(electrons, 10., 2.5, self.meta['Ele_ID'])
            # and finally for taus
            logger.debug("Applying tau preselection")
            taus = self.select_taus(taus, 18., 2.3, self.meta['Tau_ID'])

            # we sort the objects here to ensure the pairs will later be ordered preferring the most isolated particles, if objects have the same isolation then the highest pT one is preferred.
            # To ensure the correct sorting we first sort based on the least important quantity (pT), then after this we sort by the isolation

            # first sort all objects in each event descending in pt
            muons = muons[awkward.argsort(muons.pt, ascending=False)]
            electrons = electrons[awkward.argsort(electrons.pt, ascending=False)]
            taus = taus[awkward.argsort(taus.pt, ascending=False)]

            # then sort all electrons and muon by isolation (smallest isolation first)
            muons = muons[awkward.argsort(muons.pfRelIso04_all, ascending=True)]
            electrons = electrons[awkward.argsort(electrons.miniPFRelIso_all, ascending=True)]

            # also sort taus based on the RAW ID score for vs jet ID, in this case high scores correspond to 'more isolated'
            taus = taus[awkward.argsort(taus[f"raw{self.meta['Tau_ID']}VSjet"], ascending=False)]

            for ch in channels:
                pairs = {}
                logger.debug(f"Producing ditau pairs for {ch}")
                # first we make same-type pairs
                if ch == "mm":
                    mm_pairs = awkward.combinations(muons, 2, fields=["obj_1", "obj_2"])
                    pairs['mm'] = mm_pairs
                elif ch == 'ee':
                    ee_pairs = awkward.combinations(electrons, 2, fields=["obj_1", "obj_2"])
                    pairs['ee'] = ee_pairs
                elif ch == 'tt':
                    tt_pairs = awkward.combinations(taus, 2, fields=["obj_1", "obj_2"])
                    pairs['tt'] = tt_pairs

                # As preference was given to isolated particles during the sorting, for same-type pairs we need to make sure that obj_1 is always the one with the highest pT:
                if ch in ['ee','mm','tt']:
                    # Check if obj_1 has greater pt than obj_2 in each pair
                    mask = pairs[ch]["obj_1"].pt > pairs[ch]["obj_2"].pt

                    # Switch obj_1 and obj_2 if necessary
                    switched_pairs = awkward.with_field(
                        pairs[ch],
                        awkward.where(mask, pairs[ch]["obj_1"], pairs[ch]["obj_2"]),
                        "obj_1"
                    )
                    switched_pairs = awkward.with_field(
                        switched_pairs,
                        awkward.where(mask, pairs[ch]["obj_2"], pairs[ch]["obj_1"]),
                        "obj_2"
                    )
                    pairs[ch] = switched_pairs

                # combinations of mixed pairs we follow the usual HTT conventions and lighter leptons are preferred for obj_1
                # Note when using the cartesian function the pairs will be ordered preferentially by the objects passed as the first argument
                # We want to order the pairs prefering muons > electrons > taus so we must pass the arguments in the same order below

                elif ch == "em":
                    em_pairs = awkward.cartesian({"muons": muons,"electrons": electrons})
                    em_pairs = awkward.zip({"obj_1": em_pairs["electrons"], "obj_2":em_pairs["muons"]})
                    pairs['em'] = em_pairs

                elif ch == "et":
                    et_pairs = awkward.cartesian({"electrons": electrons, "taus": taus})
                    et_pairs = awkward.zip({"obj_1": et_pairs["electrons"], "obj_2":et_pairs["taus"]})
                    pairs['et'] = et_pairs

                elif ch == "mt":
                    mt_pairs = awkward.cartesian({"muons": muons, "taus": taus})
                    mt_pairs = awkward.zip({"obj_1": mt_pairs["muons"], "obj_2":mt_pairs["taus"]})
                    pairs['mt'] = mt_pairs

                p = pairs[ch]

                logger.debug("Applying channel specific selections")

                # dR cut
                dR_cut = p["obj_1"].delta_r(p["obj_2"]) > 0.5

                # pT and eta cuts
                pt_cut = (p["obj_1"].pt > self.meta['Selections'][ch]['pt_1']) & (p["obj_2"].pt > self.meta['Selections'][ch]['pt_2'])
                eta_cut = (abs(p["obj_1"].eta) < abs(self.meta['Selections'][ch]['eta_1'])) & (abs(p["obj_2"].eta) < abs(self.meta['Selections'][ch]['eta_2']))

                # extra lepton vetos
                lepton_veto_cut = ((awkward.num(electrons, axis=1) <= self.meta['Selections'][ch]['max_n_electrons'])) & ((awkward.num(muons, axis=1) <= self.meta['Selections'][ch]['max_n_muons']))

                all_cuts = dR_cut & pt_cut & eta_cut & lepton_veto_cut

                # apply masks to remove pairs that fail any of the cuts
                p = p[all_cuts]

                # finally we keep only the first pair (which is the highest ranked one)
                p = awkward.firsts(p)

                logger.debug(f"Dropping events without a selected {ch} pair")
                # drop events without a preselected pair

                selection_mask = ~awkward.is_none(p)
                pair = p[selection_mask]

                pair["weight"] = numpy.ones(len(pair))

                if self.data_kind == "mc" and len(pair) > 0:
                    pair = self.add_pair_gen_info(events, pair, selection_mask)
                    logger.debug("Applying stitching weights")
                    if self.year == "Run3_2022":
                        pair["w_DY_soup"] = numpy.ones(len(pair))
                        pair["w_WJ_soup"] = numpy.ones(len(pair))
                        do_stitch, process_id = samples_to_stitch(dataset_name)
                        if do_stitch:
                            pair[f"w_{process_id}_soup"] = stitching(self.year,events[selection_mask],process_id)
                            pair["weight"] = pair["weight"] * pair[f"w_{process_id}_soup"]

                else:
                    logger.debug("Skipping the addition of gen info to pairs as there are no surviving pairs")

                # Add pair quantities e.g visible-mass, pT etc
                pair = self.add_pair_quantities(pair)
                pair = self.add_lhe_weights(pair, events[selection_mask], dataset_name)
                pair = self.add_cp_quantities(pair, events[selection_mask], ch)

                # Now we pick up the MET and jets, and we apply the same mask that we used to select only our good pairs
                met = met_[selection_mask]
                jets = jets_[selection_mask]
                selected_jets = self.select_jets(jets,30.,4.7)
                # select b-jet candidates that pass pT and eta cuts for bjets
                selected_prebjets = self.select_jets(jets,self.meta['BTAG_ID']['pt'],self.meta['BTAG_ID']['eta'])
                # require b-jet candidates to pass cut on btagging discriminator
                selected_bjets = selected_prebjets[selected_prebjets[self.meta['BTAG_ID']['name']] > self.meta['BTAG_ID']['cut']]

                jets = selected_jets
                prebjets = selected_prebjets
                bjets = selected_bjets

                # Add jet quantities here
                # First we filter the jets that are matched in dR to our selected tau candidates
                dr_jet_obj1_cut = delta_r_mask(jets, pair['obj_1'], 0.5)
                dr_jet_obj2_cut = delta_r_mask(jets, pair['obj_2'], 0.5)
                filtered_jets = jets[dr_jet_obj1_cut & dr_jet_obj2_cut]
                # And then we compute and add our jet quantities
                pair = self.add_jet_quantities(pair,filtered_jets,"")

                # Filter jets seeding the taus
                dr_jet_obj1_seed = delta_r_seed_mask(jets, pair['obj_1'], 0.5)
                dr_jet_obj2_seed = delta_r_seed_mask(jets, pair['obj_2'], 0.5)
                filtered_jets = jets[dr_jet_obj1_seed & dr_jet_obj2_seed]
                pair = self.add_jet_quantities(pair,filtered_jets, "seeding_")

                # also add number of (pre) b-tagged jets
                # as for other jets we filter the (pre) bjets that are matched in dR to our selected tau candidates
                dr_bjet_obj1_cut = delta_r_mask(prebjets, pair['obj_1'], 0.5)
                dr_bjet_obj2_cut = delta_r_mask(prebjets, pair['obj_2'], 0.5)
                filtered_prebjets = prebjets[dr_bjet_obj1_cut & dr_bjet_obj2_cut]
                # for now we don't use a dedicated function for bjet quantities as we only store n_bjets but this
                # could be added if we eventually want to store more information
                pair["n_prebjets"] = awkward.num(filtered_prebjets)

                # also add number of b-tagged jets
                # as for other jets we filter the bjets that are matched in dR to our selected tau candidates
                dr_bjet_obj1_cut = delta_r_mask(bjets, pair['obj_1'], 0.5)
                dr_bjet_obj2_cut = delta_r_mask(bjets, pair['obj_2'], 0.5)
                filtered_bjets = bjets[dr_bjet_obj1_cut & dr_bjet_obj2_cut]
                # for now we don't use a dedicated function for bjet quantities as we only store n_bjets but this
                # could be added if we eventually want to store more information
                pair["n_bjets"] = awkward.num(filtered_bjets)

                # Add MET quantities e.g MET, transverse masses etc
                pair = self.add_met_quantities(pair,met)

                # Add Lumi Block, Run and Event number
                pair["event"] = events.event[selection_mask]
                pair["run"] = events.run[selection_mask]
                pair["lumi"] = events.luminosityBlock[selection_mask]

                # Get trigger objects
                trig_objects = events.TrigObj[selection_mask]
                # Apply trigger matching which adds booleons to the array indicating whether the pair legs were matched to particular trigger objects
                if 'Triggers' in self.meta and ch in self.meta['Triggers']:
                    logger.debug("Determining trigger decisions")
                    triggers = self.meta['Triggers'][ch]
                    # loop over all specified triggers and store a booleon for each one indicating which events passed the trigger
                    for trig_name, trig_sels in triggers.items():
                        leg1_selections = trig_sels['leg1'] if 'leg1' in trig_sels else None
                        leg2_selections = trig_sels['leg2'] if 'leg2' in trig_sels else None
                        leg3_selections = trig_sels['leg3'] if 'leg3' in trig_sels else None
                        leg4_selections = trig_sels['leg4'] if 'leg4' in trig_sels else None
                        pair = self.apply_triggers(trig_objects, pair, trig_name, leg1_selections, leg2_selections, leg3_selections, leg4_selections)
                else:
                    logger.debug("Not requiring any triggers as none were specified in the metaconditions")

                # workflow specific processing
                events, process_extra = self.process_extra(events)
                histos_etc.update(process_extra)

                # continue if there is no surviving events
                if len(pair) == 0:
                    logger.debug(f"No surviving {ch} channel events in this run, no output will be written!")
                    continue

                if self.data_kind == "mc":
                    for correction_name in correction_names:
                        if correction_name in available_weight_corrections:
                            event_weights = Weights(size=len(events[selection_mask]))
                            logger.info(
                                f"Adding correction {correction_name} to weight collection of dataset {dataset_name}"
                            )
                            varying_function = available_weight_corrections[correction_name]
                            event_weights = varying_function(
                                events=events[selection_mask],
                                weights=event_weights,
                                pair=pair,
                                channel=ch,
                                dataset_name=dataset_name,
                                year=self.year,
                            )
                            pair["w_" + correction_name] = event_weights.weight()
                            pair["weight"] = pair["weight"] * event_weights.weight()

                if self.output_location is not None:
                    akarr = ditau_ak_array(self, pair)
                    akarr = self.rename_and_drop(akarr, ch, self.data_kind, correction_names)
                    fname = (
                        events.behavior["__events_factory__"]._partition_key.replace(
                            "/", "_"
                        )
                        + ".parquet"
                    )
                    subdirs = [ch]
                    if "dataset" in events.metadata:
                        subdirs.append(events.metadata["dataset"])
                    subdirs.append(variation)
                    dump_ak_array(self, akarr, fname, self.output_location + "/" + self.year, metadata, subdirs)

        return histos_etc

    def postprocess(self, accumulant: Dict[Any, Any]) -> Any:
        raise NotImplementedError


class DiTauProcessor(HtautauBaseProcessor):
    def __init__(
        self,
        metaconditions: Dict[str, Any],
        run_effective: bool = False,
        systematics: Optional[List[Any]] = None,
        corrections: Optional[List[Any]] = None,
        apply_trigger: bool = False,
        output_location: Optional[str] = None,
        taggers: Optional[List[Any]] = None,
        year: str = '',
        analysis: str = '',
    ) -> None:
        super().__init__(
            metaconditions,
            run_effective=run_effective,
            systematics=systematics,
            corrections=corrections,
            apply_trigger=apply_trigger,
            output_location=output_location,
            taggers=taggers,
            year=year,
            trigger_group=".*SingleMu.*",
            analysis="mainAnalysis",
        )
        self.trigger_group = ".*SingleMu.*"
        self.analysis = "mainAnalysis"

    def process_extra(self, events: awkward.Array) -> awkward.Array:
        return events, {}

    def postprocess(self, accumulant: Dict[Any, Any]) -> Any:
        pass
