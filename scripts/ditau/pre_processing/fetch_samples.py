import yaml
import json
import subprocess
import os

# Load the YAML file
with open('scripts/ditau/pre_processing/samples.yaml', 'r') as file:
    data = yaml.safe_load(file)


# Function to get file paths using gfal-ls
def get_file_paths(year,sample_name):
    command = f"gfal-ls davs://gfe02.grid.hep.ph.ic.ac.uk:2880/pnfs/hep.ph.ic.ac.uk/data/cms/store/user/ksavva/Run3/{year}/{sample_name}"
    try:
        result = subprocess.run(command, shell=True, capture_output=True, text=True, check=True)
        files = result.stdout.splitlines()
        paths = [f"root://gfe02.grid.hep.ph.ic.ac.uk:1097//store/user/ksavva/Run3/{year}/{sample_name}/{file}" for file in files if file.endswith(".root")]
        return paths
    except subprocess.CalledProcessError as e:
        print(f"An error occurred while processing {sample_name}: {e}")
        return []


# Iterate over the YAML data to populate the JSON structure
for year, categories in data.items():
    print(f"Processing year {year}...")
    json_structure = {}
    for category, samples in categories.items():
        for sample in samples:
            print(f"Processing {sample}...")
            json_structure[sample] = get_file_paths(year,sample)
    json_output = json.dumps(json_structure, indent=4)
    # Save to a JSON file
    if not os.path.exists(f'scripts/ditau/config/{year}'):
        os.makedirs(f'scripts/ditau/config/{year}')
    with open(f'scripts/ditau/config/{year}/samples.json', 'w') as json_file:
        json_file.write(json_output)
    print(f"JSON file created successfully for {year}.")
